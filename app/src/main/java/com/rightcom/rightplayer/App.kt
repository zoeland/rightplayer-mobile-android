package com.rightcom.rightplayer

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.crashlytics.android.Crashlytics
import com.hypertrack.hyperlog.HyperLog
import com.rightcom.rightplayer.helpers.SocketHelper
import com.rightcom.rightplayer.models.Player
import com.rightcom.rightplayer.models.Prefs
import com.rightcom.rightplayer.services.LogFileArchiveScheduler
import com.rightcom.rightplayer.tools.Constant
import com.rightcom.rightplayer.tools.Constant.i
import com.rightcom.rightplayer.tools.CustomLogMessageFormat
import com.rightcom.rightplayer.tools.ExceptionHandler
import com.rightcom.rightplayer.tools.Utils.pushLogsOnElasticSearch
import com.rightcom.rightplayer.webservice.WebService
import com.rightcom.rightplayer.webservice.WebServiceUtils
import io.fabric.sdk.android.Fabric
import io.socket.client.Socket
import java.util.*
import com.crashlytics.android.core.CrashlyticsCore
import com.crashlytics.android.answers.Answers

val app: App by lazy {
    App.app!!
}
val appContext: Context by lazy {
    App.context!!
}
val prefs: Prefs by lazy {
    App.prefs!!
}
val webService: WebService by lazy {
    App.webService!!
}
val player: Player by lazy {
    App.player!!
}
val socket: Socket by lazy {
    App.socket
}

class App : Application() {
    private val TAG = App::class.java.simpleName
    
    override fun onCreate() {
        super.onCreate()
//        Fabric.with(this, Crashlytics()) // init fabric
        Fabric.with(this, Answers(), Crashlytics.Builder().core(CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build()).build())
        // Second, set custom UncaughtExceptionHandler
        val mDefaultUEH = Thread.getDefaultUncaughtExceptionHandler()
        val exceptionHandler = ExceptionHandler(this, mDefaultUEH)
        Thread.setDefaultUncaughtExceptionHandler(exceptionHandler)
        setupActivityListener()
        app = this@App
        context = applicationContext
        prefs = Prefs(applicationContext)
        webService = WebServiceUtils.webserviceManager
        player = Player.instance
        SocketHelper.connect()

        // Configure app logs
        HyperLog.initialize(appContext, CustomLogMessageFormat(appContext))
        HyperLog.setLogLevel(Log.VERBOSE)
        // Set Alarm for app log file archive
        setAlarmForZipAppLogsFile()

        HyperLog.i(TAG, "Application started successfully. : ${TAG.toUpperCase()}")
        pushLogsOnElasticSearch(i,"Application started successfully. : ${TAG.toUpperCase()}")
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        var app: App? = null
        @SuppressLint("StaticFieldLeak")
        var context: Context? = null
        var prefs: Prefs? = null
        var webService: WebService? = null
        var player: Player? = null
        var socket = SocketHelper.socket
    }

    /**
     * Function used to set app activities listeners
     */
    private fun setupActivityListener() {
        registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks{
            override fun onActivityPaused(activity: Activity?) {}
            override fun onActivityResumed(activity: Activity?) {}
            override fun onActivityStarted(activity: Activity?) {}
            override fun onActivityDestroyed(activity: Activity?) {}
            override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {}
            override fun onActivityStopped(activity: Activity?) {}
            override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {}
        })
        HyperLog.i(TAG, "Set up application listener. : ${TAG.toUpperCase()}")
        pushLogsOnElasticSearch(i,"Set up application listener. : ${TAG.toUpperCase()}")
    }

    /**
     * Setting up the alarm for log file archive scheduler
     */
    private fun setAlarmForZipAppLogsFile() {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH),
                    8, 30, 0)
        } else {
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH),
                    8, 30, 0)
        }
        //getting the alarm manager
        val alarm = getSystemService(Context.ALARM_SERVICE) as AlarmManager

        //creating a new intent specifying the broadcast receiver
        val i = Intent(this, LogFileArchiveScheduler::class.java)

        //creating a pending intent using the intent
        val pi = PendingIntent.getBroadcast(this, 0, i, 0)

        //setting the repeating alarm that will be fired every day
        alarm.setRepeating(AlarmManager.RTC, calendar.timeInMillis, AlarmManager.INTERVAL_DAY, pi)

        HyperLog.i(TAG, "Set up alarm to schedule zipping app log file. : ${TAG.toUpperCase()}")
        pushLogsOnElasticSearch(Constant.i,"Set up alarm to schedule zipping app log file. : ${TAG.toUpperCase()}")
    }
}