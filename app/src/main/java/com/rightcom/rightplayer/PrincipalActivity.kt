package com.rightcom.rightplayer

import android.app.ActivityManager
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.hypertrack.hyperlog.HyperLog
import com.jaredrummler.android.device.DeviceName
import com.rightcom.rightplayer.fragments.CampaignFragment
import com.rightcom.rightplayer.fragments.PlayerStateFragment
import com.rightcom.rightplayer.fragments.SplashScreenFragment
import com.rightcom.rightplayer.fragments.WatchDemoFragment
import com.rightcom.rightplayer.helpers.SocketHelper
import com.rightcom.rightplayer.models.Models
import com.rightcom.rightplayer.models.Player
import com.rightcom.rightplayer.tools.Constant
import com.rightcom.rightplayer.tools.Constant.ACTION_GET_INFO
import com.rightcom.rightplayer.tools.Constant.ACTION_PING
import com.rightcom.rightplayer.tools.Constant.ACTION_SHUTDOWN
import com.rightcom.rightplayer.tools.Constant.DEVICE_OS
import com.rightcom.rightplayer.tools.Constant.EMIT_DEACTIVATE
import com.rightcom.rightplayer.tools.Constant.EMIT_IP_ADDRESS
import com.rightcom.rightplayer.tools.Constant.EMIT_MY_PING
import com.rightcom.rightplayer.tools.Constant.EMIT_OS_STATUS
import com.rightcom.rightplayer.tools.Constant.EMIT_PLAYING
import com.rightcom.rightplayer.tools.Constant.EMIT_READY
import com.rightcom.rightplayer.tools.Constant.MODE_OFFLINE
import com.rightcom.rightplayer.tools.Constant.MODE_ONLINE
import com.rightcom.rightplayer.tools.Constant.ON_CHANGE_MODE
import com.rightcom.rightplayer.tools.Constant.ON_GET_IP_SERVER
import com.rightcom.rightplayer.tools.Constant.ON_MOVETO
import com.rightcom.rightplayer.tools.Constant.ON_PLAY_NOW
import com.rightcom.rightplayer.tools.Constant.ON_REFRESH
import com.rightcom.rightplayer.tools.Constant.ON_RELAUNCH
import com.rightcom.rightplayer.tools.Constant.ON_RELOAD
import com.rightcom.rightplayer.tools.Constant.ON_SCHEDULE_DELETE_INFO
import com.rightcom.rightplayer.tools.Constant.ON_SCHEDULE_INFO
import com.rightcom.rightplayer.tools.Constant.ON_SCHEDULE_UPDATE_INFO
import com.rightcom.rightplayer.tools.Constant.ON_SEND_COMMAND
import com.rightcom.rightplayer.tools.Constant.ON_UPDATE_PLAYER
import com.rightcom.rightplayer.tools.Constant.PLAYER
import com.rightcom.rightplayer.tools.Constant.i
import com.rightcom.rightplayer.tools.CustomAppCompatActivity
import com.rightcom.rightplayer.tools.Utils
import com.rightcom.rightplayer.tools.Utils.deviceHasInternetAccess
import com.rightcom.rightplayer.tools.Utils.fromJsonArray
import com.rightcom.rightplayer.tools.Utils.fromJsonToCampaign
import com.rightcom.rightplayer.tools.Utils.writeLog
import com.rightcom.rightplayer.tools.Utils.pushLogsOnElasticSearch
import com.rightcom.rightplayer.tools.Utils.loadFragment
import com.rightcom.rightplayer.tools.Utils.removePlayerData
import com.rightcom.rightplayer.webservice.WebServiceUtils
import io.socket.client.Ack
import kotlinx.android.synthetic.main.activity_principal.*
import org.apache.commons.lang3.StringUtils
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.HttpURLConnection
import java.sql.Timestamp
import java.util.*


class PrincipalActivity :
        CustomAppCompatActivity(),
        SocketHelper.SocketListener,
        SplashScreenFragment.OnSplashScreenFragmentInteraction,
        PlayerStateFragment.OnPlayerStateFragmentListener,
        CampaignFragment.CampaignFragmentInteraction {


    private val HLTAG = PrincipalActivity::class.java.simpleName
    private var loadWatchDemo = false

    private var queueIsRunning = false
    private var campaignQueue = HashMap<Long, Models.Campaign?>()

    private var playNowCampaignQueue = HashMap<Long, Models.Campaign?>()
    private var playNowQueueIsRunning = false

    /**
     * Relaunch app with this function
     */
    private val relaunchApp = {
        writeLog("Relaunch player", "info")
        HyperLog.i(HLTAG, "Set up relaunch player property. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i,"Set up relaunch player property. : ${HLTAG.toUpperCase()}")

        val intent = Intent(this@PrincipalActivity, PrincipalActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val pendingIntent = PendingIntent.getActivity(app.baseContext, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val mgr = app.baseContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        mgr.set(AlarmManager.RTC, System.currentTimeMillis()+100, pendingIntent)
        this@PrincipalActivity.finish()
        System.exit(2)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_principal)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON) // To keep screen alive

        removeAllFragment()
        loadFragment(supportFragmentManager, SplashScreenFragment(), SplashScreenFragment.HLTAG, false)
    }

    override fun requestCodeChecking() {
        HyperLog.i(SplashScreenFragment.HLTAG, "Request player data checking. : ${SplashScreenFragment.HLTAG.toUpperCase()}")
        Utils.pushLogsOnElasticSearch(Constant.i, "Request player data checking. : ${SplashScreenFragment.HLTAG.toUpperCase()}")
        checkPlayerData()
    }

    override fun requestGetIpAndCodeGeneration() {
        HyperLog.i(SplashScreenFragment.HLTAG, "Call function to get device ip address and go to generate player data. : ${SplashScreenFragment.HLTAG.toUpperCase()}")
        Utils.pushLogsOnElasticSearch(Constant.i, "Call function to get device ip address and go to generate player data. : ${SplashScreenFragment.HLTAG.toUpperCase()}")
        getIpAddressAndGoToGeneratePlayerData()
    }

    override fun handleConnectionError() {
        Utils.writeLog("RightPlayer network error.", "error")
        HyperLog.e(SplashScreenFragment.HLTAG,"RightPlayer network error. : ${SplashScreenFragment.HLTAG.toUpperCase()}")
        Utils.pushLogsOnElasticSearch(Constant.i, "RightPlayer network error. : ${SplashScreenFragment.HLTAG.toUpperCase()}")
        updateView()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        for (i in CHANNEL) {
            socket.off(i)
            Log.e("OFF", i)
        }
        Log.e(HLTAG, "Activity onDestroy")
    }

    override fun onStop() {
        super.onStop()
        Log.e(HLTAG, "Activity onStop")
    }

    override fun onBackPressed() {
        if (loadWatchDemo) {
            removeAllFragment()
            loadFragment(supportFragmentManager, PlayerStateFragment(), PlayerStateFragment.HLTAG, false)
        } else {
            this@PrincipalActivity.finish()
        }
    }

    override fun finishToLoadCampaign(campaignKey: Long, campaignName: String, loadingFailed: Boolean) {
        when (prefs.campaign?.from_v2) {
            true -> { // New version "play_now" process
                playNowCampaignQueue.remove(campaignKey)
                if (loadingFailed) {
                    if (playNowCampaignQueue.isNotEmpty()) {
                        playNowStartQueue()
                    }
                } else {
                    playNowStartQueue()
                }
            }
            false -> { // Default process
                campaignQueue.remove(campaignKey)
                if (loadingFailed) {
                    if (campaignQueue.isNotEmpty()) {
                        startQueue()
                    }
                } else {
                    startQueue()
                }
            }
        }
    }

    /**
     * Remove all fragment in container (PrincipalActivity) if fragment
     * exist inside container
     */
    private fun removeAllFragment() {
        val size = supportFragmentManager.fragments.size
//        Log.e("SIZE", "$size")
        for (fragment in supportFragmentManager.fragments) {
            if (fragment != null) {
                supportFragmentManager.beginTransaction().remove(fragment).commit()
            }
        }

    }

    /**
     * Reload current view of player with this function
     */
    private fun reloadApp() {
        // Log
        finish()
        startActivity(intent)
    }

    override fun onClickWatchDemo() {
        loadWatchDemo = true
        removeAllFragment()
        loadFragment(supportFragmentManager, WatchDemoFragment(), WatchDemoFragment.HLTAG , false)
    }

    override fun onReceiveEvent(channel: String, data: Any) {
        Log.e("ON_RECEIVE_EVENT", GsonBuilder().create().toJson(data))
        HyperLog.i(HLTAG, "${GsonBuilder().create().toJson(data)} : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i,"${GsonBuilder().create().toJson(data)} : ${HLTAG.toUpperCase()}")
        val obj = fromJsonArray(GsonBuilder().create().toJson(data))
        updateSocketOnInPrefs(obj)

        // Find action to do when socket event received
        when(channel) {
            ON_RELOAD -> {
                Log.e(HLTAG, "Receive on channel $ON_RELOAD")
                reloadApp()
            }
            ON_MOVETO -> changeAppState(obj, data)
            ON_REFRESH -> {
                Log.e(HLTAG, "Receive on channel $ON_REFRESH")
                reloadApp()
            }
            ON_RELAUNCH -> relaunchApp()
            ON_CHANGE_MODE -> changePlayerMode(obj)
            ON_SEND_COMMAND -> onSendCommand(obj)
            ON_GET_IP_SERVER -> getIpAddress(obj)
            ON_UPDATE_PLAYER -> Log.e(SocketHelper.TAG, "On receive event on channel $channel")
            ON_SCHEDULE_INFO -> Log.e(SocketHelper.TAG, "On receive event on channel $channel")
            ON_SCHEDULE_DELETE_INFO -> Log.e(SocketHelper.TAG, "On receive event on channel $channel")
            ON_SCHEDULE_UPDATE_INFO -> Log.e(SocketHelper.TAG, "On receive event on channel $channel")
            ON_PLAY_NOW -> {startPlayNowProcess(data, channel)}
        }
    }

    /**
     * This function change player state according to the status
     * @param obj
     * @param args
     */
    private fun changeAppState(obj: Models.NameValuePairs, args: Any) {
        Log.e(SocketHelper.TAG, "Receive on moveto")
        HyperLog.i(HLTAG, "Receive socket on channel moveto. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i,"Receive socket on channel moveto. : ${HLTAG.toUpperCase()}")
        prefs.playerStatus = obj.status?.toUpperCase() // Store player state in player singleton
        player.publickey = obj.publickey // Store public key in singleton

        // Prepare data to emit response in active or unactive case
        val data = JSONObject()
        data.put("player_code",  obj.player_code)
        data.put("playerid", obj.playerid)
        data.put("publickey",  obj.publickey)

//        Log.e("DATA", data.toString())

        // Find action to do according to the status
        when(obj.status?.toUpperCase()) {
            Player.State.ACTIVE.name -> {
                socket.emit(EMIT_READY, data)
                if (!obj.mode.isNullOrEmpty() && obj.mode != prefs.playerMode) prefs.playerMode = obj.mode
                removeAllFragment()
                loadFragment(supportFragmentManager, PlayerStateFragment(), PlayerStateFragment.HLTAG, false)

                // Log
                Log.e(SocketHelper.TAG, "Emit on ready")
                writeLog("Player is ${obj.status?.toLowerCase()}", "info")
                HyperLog.i(HLTAG, "Player's mode change to ${obj.status?.toLowerCase()}. : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i,"Player's mode change to ${obj.status?.toLowerCase()}. : ${HLTAG.toUpperCase()}")
            }
            Player.State.UNACTIVE.name -> {
                socket.emit(EMIT_DEACTIVATE, data)
                removeAllFragment()
                loadFragment(supportFragmentManager, PlayerStateFragment(), PlayerStateFragment.HLTAG, false)

                // Log
                Log.e(SocketHelper.TAG, "Emit on unactive")
                writeLog("Player is ${obj.status?.toLowerCase()}", "info")
                HyperLog.i(HLTAG, "Player's mode change to ${obj.status?.toLowerCase()}. : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i,"Player's mode change to ${obj.status?.toLowerCase()}. : ${HLTAG.toUpperCase()}")
            }
            Player.State.DELETED.name -> {
                removePlayerData()

                // Log
                Log.e(SocketHelper.TAG, "Receive on deleted")
                writeLog("Player is ${obj.status?.toLowerCase()}", "info")
                HyperLog.i(HLTAG, "Player's status change to ${obj.status?.toLowerCase()}. : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i,"Player's status change to ${obj.status?.toLowerCase()}. : ${HLTAG.toUpperCase()}")

                // Reload app
                reloadApp()
            }
            Player.State.PLAYING.name -> {
                Log.e(HLTAG, "PrincipalActivity socket channel moveto action PLAYING.")
                Log.e(SocketHelper.TAG, "Receive on playing")
                writeLog("Player received action to play campaign ${prefs.socketOn?.status?.toLowerCase()}", "info")
                HyperLog.i(HLTAG, "Player's status change to ${prefs.socketOn?.status?.toLowerCase()}. : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i,"Player's status change to ${prefs.socketOn?.status?.toLowerCase()}. : ${HLTAG.toUpperCase()}")

                val campaign = fromJsonToCampaign(GsonBuilder().create().toJson(args))
                campaignQueue[Timestamp(System.currentTimeMillis()).time] = campaign

                Log.e("REQUEST PLAYING", "${campaign?.campaign_name}")
                if (!queueIsRunning) {
                    Log.e("CAMPAIGN QUEUE", "Not running, we start it")
                    startQueue()
                }
            }
        }
    }

    /**
     * Function to execute action when receive send command
     * @param obj
     */
    private fun onSendCommand(obj: Models.NameValuePairs) {
        when(obj.action?.toUpperCase()) {
            ACTION_PING.toUpperCase() -> pingAction(obj)
            ACTION_GET_INFO.toUpperCase() -> deviceInfo(obj)
            ACTION_SHUTDOWN.toUpperCase() -> shutdownAction()
        }
    }

    /**
     * Function to change player mode
     * @param obj
     */
    private fun changePlayerMode(obj: Models.NameValuePairs) {
        Log.e(SocketHelper.TAG, "Receive on change mode")
        HyperLog.i(SocketHelper.TAG, "Receive socket on channel change mode. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i,"Receive socket on channel change mode. : ${HLTAG.toUpperCase()}")
        prefs.playerMode = obj.mode
        val data = JSONObject()
        data.put("player_code", obj.player_code)
        data.put("playerid", obj.player_code)
        data.put("publickey", obj.publickey)
        data.put("event", "room_${obj.publickey}")
        data.put("status", StringUtils.capitalize(prefs.playerStatus?.toLowerCase()))
        data.put("mode", obj.mode)
        socket.emit(EMIT_MY_PING, data)

        // Log
        Log.e(SocketHelper.TAG, "Emit on myping")
        writeLog("Player change mode to ${obj.mode}", "info")
        HyperLog.i(HLTAG, "Player's mode change to ${obj.mode}. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i,"Player's mode change to ${obj.mode}. : ${HLTAG.toUpperCase()}")
    }

    /**
     * Function to get ip address by api calling
     * @param obj
     */
    private fun getIpAddress(obj: Models.NameValuePairs) {
        Log.e(SocketHelper.TAG, "Receive on get_ip_server")
        HyperLog.i(HLTAG, "Receive on channel get_ip_server. Get device IP address. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i,"Receive on channel get_ip_server. Get device IP address. : ${HLTAG.toUpperCase()}")
        val webService = WebServiceUtils.webserviceManager(Constant.IP_LOCATION_URL)
        val call = webService.userLocation
        call.enqueue(object : Callback<Models.IpInfo> {
            override fun onResponse(call: Call<Models.IpInfo>?, response: Response<Models.IpInfo>?) {
                if (response?.code() == HttpURLConnection.HTTP_OK && response.body() != null) {
                    val data = JSONObject()
                    data.put("player_code", obj.player_code)
                    data.put("playerid", obj.playerid)
                    data.put("publickey", obj.publickey)
                    data.put("event", obj.event)
                    data.put("ip_adress", response.body()?.query)
                    socket.emit(EMIT_IP_ADDRESS, data)

                    // Log
                    Log.e(SocketHelper.TAG, "Emit on ip_address")
                    writeLog("Player send ip address", "info")
                    HyperLog.i(HLTAG, "Player emit the device ip address. : ${HLTAG.toUpperCase()}")
                    pushLogsOnElasticSearch(i,"Player emit the device ip address. : ${HLTAG.toUpperCase()}")
                }
            }

            override fun onFailure(call: Call<Models.IpInfo>?, t: Throwable?) {

            }
        })
    }

    /**
     * Function to get player info
     */
    private fun playerInfo(): JSONObject {
        Log.e("DEVICE_NAME", DeviceName.getDeviceName())
        val deviceData = JSONObject()
        deviceData.put("device", DeviceName.getDeviceName())
        deviceData.put("playertype", PLAYER)
        deviceData.put("player", "Android Player")
        deviceData.put("os", DEVICE_OS)
        deviceData.put("ram", "${usedMemoryPercent()}%")
        deviceData.put("local", Locale.getDefault().displayLanguage)
        deviceData.put("boottime", SystemClock.uptimeMillis())
        return deviceData
    }

    /**
     * Function to calculate used memory percentage
     */
    private fun usedMemoryPercent(): Int {
        val activityManager = appContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val memoryInfo = ActivityManager.MemoryInfo()
        activityManager.getMemoryInfo(memoryInfo)
        val um = (((memoryInfo.totalMem - memoryInfo.availMem)*100)/memoryInfo.totalMem.toFloat()).toInt()
        return um
    }

    /**
     * Function to do action when ping requested
     * @param obj
     */
    private fun pingAction(obj: Models.NameValuePairs) {
        val emitData = JSONObject()
        emitData.put("player_code", obj.player_code)
        emitData.put("playerid", obj.playerid)
        emitData.put("publickey", obj.publickey)
        emitData.put("status", StringUtils.capitalize(prefs.playerStatus?.toLowerCase()))
        emitData.put("event", obj.event)
        emitData.put("mode",  if (obj.mode != null) MODE_OFFLINE else MODE_ONLINE)
        socket.emit(EMIT_MY_PING, emitData)

        // Log
        Log.e(SocketHelper.TAG, "Emit on myping")
        writeLog("Player response to ping action", "info")
        HyperLog.i(HLTAG, "Player response to ping action. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i,"Player response to ping action. : ${HLTAG.toUpperCase()}")
    }

    /**
     * Function to get device information
     * @param obj
     */
    private fun deviceInfo(obj: Models.NameValuePairs) {
        val data = JSONObject()
        data.put("player_code", obj.player_code)
        data.put("playerid", obj.playerid)
        data.put("publickey", obj.publickey)
        data.put("event", obj.event)
        data.put("playerinfo", playerInfo())
        socket.emit(EMIT_OS_STATUS, data)

        // Log
        Log.e(SocketHelper.TAG, "Emit on get_info")
        writeLog("Player response to get info", "info")
        HyperLog.i(HLTAG,"Player response to get info. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i,"Player response to get info. : ${HLTAG.toUpperCase()}")
    }

    /**
     * Function to make shutdown
     */
    private fun shutdownAction() {
        // TODO: Shutdown the device
        Log.e(SocketHelper.TAG, "Action to shutdown")
    }

    /**
     * When receive an socket event, we updated shared preferences
     * "socketOn" data
     */
    private fun updateSocketOnInPrefs(obj: Models.NameValuePairs) {
        var prefObj = prefs.socketOn
        if (prefObj != null) {
            if (obj.playerid != null && obj.playerid != prefObj.playerid) {
                prefObj.player_code = obj.playerid
            }
            if (obj.publickey != null && obj.publickey != prefObj.publickey) {
                prefObj.publickey = obj.publickey
            }
            if (obj.version != null && obj.version != prefObj.version) {
                prefObj.version = obj.version
            }
            if (obj.action != null && obj.action != prefObj.action) {
                prefObj.action = obj.action
            }
            if (obj.player_code != null && obj.player_code != prefObj.player_code) {
                prefObj.player_code = obj.player_code
            }
            if (obj.event != null && obj.event != prefObj.event) {
                prefObj.event = obj.event
            }
            if (obj.reload_at != null && obj.reload_at != prefObj.reload_at) {
                prefObj.reload_at = obj.reload_at
            }
            if (obj.relaunch_at != null && obj.relaunch_at != prefObj.relaunch_at) {
                prefObj.relaunch_at = obj.relaunch_at
            }
            if (obj.status != null && obj.status != prefObj.status) {
                prefObj.status = obj.status
            }
            if (obj.mode != null && obj.mode != prefObj.mode) {
                prefObj.mode = obj.mode
            }
        } else {
            prefObj = obj
        }
        Log.e("UPDATE_SOCKET_IN_PREFS", GsonBuilder().create().toJson(prefObj))
        HyperLog.i(HLTAG, "${GsonBuilder().create().toJson(prefObj)}. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i,"${GsonBuilder().create().toJson(prefObj)}. : ${HLTAG.toUpperCase()}")
        prefs.socketOn = prefObj
    }

    /**
     * This function call api to get device ip
     */
    private fun getIpAddressAndGoToGeneratePlayerData() {
        val webService = WebServiceUtils.webserviceManager(Constant.IP_LOCATION_URL)
        val call = webService.userLocation
        call.enqueue(object : Callback<Models.IpInfo> {
            override fun onFailure(call: Call<Models.IpInfo>?, t: Throwable?) {
                HyperLog.i(HLTAG, "Failure on get local ip address. : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "Failure on get local ip address. : ${HLTAG.toUpperCase()}")
                handleError(getString(R.string.text_error_connection))
            }

            override fun onResponse(call: Call<Models.IpInfo>?, response: Response<Models.IpInfo>?) {
                if (response?.code() == HttpURLConnection.HTTP_OK && response.body() != null) {
                    HyperLog.i(HLTAG, "Success on get local ip address. : ${HLTAG.toUpperCase()}")
                    pushLogsOnElasticSearch(i, "Success on get local ip address. : ${HLTAG.toUpperCase()}")
                    generatePlayerData(response.body()?.query)
                } else {
                    HyperLog.i(HLTAG, "${getString(R.string.text_internal_server_error)} : ${HLTAG.toUpperCase()}")
                    pushLogsOnElasticSearch(i, "${getString(R.string.text_internal_server_error)} : ${HLTAG.toUpperCase()}")
                    handleError(getString(R.string.text_internal_server_error))
                }
            }
        })
    }

    /**
     * In this function, we call api to check player data
     * @param ip
     */
    private fun generatePlayerData(ip: String?) {

        val call = webService.generateCode(
                playerid = Utils.uniqueID(), action = Constant.ACTION_GENERATE, device = Constant.DEVICE_NAME,
                player = PLAYER, os = DEVICE_OS, playertype = Constant.PLAYER_TYPE, signature = Constant.SIGNATURE,
                ip = ip, versionCode = Constant.VERSION_CODE, versionName = Constant.VERSION_NAME, releaseDate = Constant.RELEASE_DATE)
        call.enqueue(object : Callback<Any>{
            override fun onFailure(call: Call<Any>?, t: Throwable?) {
                HyperLog.i(HLTAG, " Failure generating player code. : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "Failure generating player code. : ${HLTAG.toUpperCase()}")
                handleError(getString(R.string.text_error_connection))
            }

            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                if (response?.code() == HttpURLConnection.HTTP_OK && response.body() != null) {
                    HyperLog.i(HLTAG, "Success on generate player's code. : ${HLTAG.toUpperCase()}")
                    pushLogsOnElasticSearch(i, "Success on generate player's code. : ${HLTAG.toUpperCase()}")
                    val mResponse = Utils.objectMapper.convertValue(response.body(), Map::class.java)
                    onRequestSuccess(mResponse)
                } else {
                    handleError(getString(R.string.text_internal_server_error))
                }
            }
        })
    }

    /**
     * In this function, we call api to check player data
     */
    private fun checkPlayerData() {
        val call = webService.checkCode(
                playerid = prefs.playerId!!, action = Constant.ACTION_CHECK, signature = Constant.SIGNATURE,
                versionCode = Constant.VERSION_CODE, versionName = Constant.VERSION_NAME, releaseDate = Constant.RELEASE_DATE)
        call.enqueue(object : Callback<Any>{
            override fun onFailure(call: Call<Any>?, t: Throwable?) {
                HyperLog.i(HLTAG, "Failure checking player's data. : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "Failure checking player's data. : ${HLTAG.toUpperCase()}")
                handleError(getString(R.string.text_error_connection))
            }

            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                if (response?.code() == HttpURLConnection.HTTP_OK && response.body() != null) {
                    HyperLog.i(HLTAG, "Success checking player's data. : ${HLTAG.toUpperCase()}")
                    pushLogsOnElasticSearch(i, "Success checking player's data. : ${HLTAG.toUpperCase()}")
                    val mResponse = Utils.objectMapper.convertValue(response.body(), Map::class.java)
                    onRequestSuccess(mResponse)
                } else {
                    handleError(getString(R.string.text_internal_server_error))
                }
            }
        })
    }

    /**
     * Perform an action to do when player request success
     * @param response: Server's response convert to a Map
     */
    private fun onRequestSuccess(response: Map<*, *>) {
        val status = (response["status"] as Double).toInt()
        val data = Utils.objectMapper.convertValue(response["data"], Map::class.java)
        when(status) {
            HttpURLConnection.HTTP_OK -> { // Generate player code
                prefs.playerCode = data["player_code"] as String
                prefs.playerStatus = (data["status"] as String).toUpperCase()
                prefs.playerId = data["playerid"] as String
                if (data.containsKey("voiceticketactivated"))
                    prefs.voiceticketactivated = (data["voiceticketactivated"] as Double).toInt()
                if (data.containsKey("mode"))
                    prefs.playerMode = data["mode"] as String?

                findNextStep()
            }
            HttpURLConnection.HTTP_CREATED -> { // Activate player from dashboard
                prefs.playerCode = data["player_code"] as String
                player.publickey = data["publickey"] as String?
                prefs.playerId = data["playerid"] as String
                prefs.playerStatus = (data["status"] as String).toUpperCase()
                prefs.voiceticketactivated = (data["voiceticketactivated"] as Double).toInt()
                prefs.playerMode = data["mode"] as String?
                findNextStep()
            }
            HttpURLConnection.HTTP_ACCEPTED, HttpURLConnection.HTTP_NOT_AUTHORITATIVE -> { // RightPlayer's player playing an campaign
                prefs.playerCode = data["player_code"] as String
                prefs.playerStatus = (data["status"] as String).toUpperCase()
                prefs.playerId = data["playerid"] as String
                player.publickey = data["publickey"] as String?
                prefs.voiceticketactivated = (data["voiceticketactivated"] as Double).toInt()
                prefs.campaign = Utils.gsonBuilder.fromJson(Utils.gsonBuilder.toJson(data["campaign"]), Models.Campaign::class.java)
                prefs.playerMode = data["mode"] as String?

                findNextStep()
            }
            HttpURLConnection.HTTP_UNAUTHORIZED -> { // RightPlayer's player is unactivated
                prefs.playerCode = data["player_code"] as String
                prefs.voiceticketactivated = (data["voiceticketactivated"] as Double).toInt()
                player.publickey = data["publickey"] as String?
                prefs.playerMode = data["mode"] as String?
                prefs.playerStatus = (data["status"] as String).toUpperCase()

                val json = JSONObject()
                json.put("status", status)
                json.put("message", response["message"] as String)
                HyperLog.i(HLTAG, "Response $json / success info")
                pushLogsOnElasticSearch(i, "Response $json / success info")

                findNextStep()
            }
            HttpURLConnection.HTTP_NOT_FOUND -> { // RightPlayer's player doesn't exist in external db
                val json = JSONObject()
                json.put("status", status)
                json.put("message", response["message"] as String)
                HyperLog.i(HLTAG, "Response $json / success info")
                pushLogsOnElasticSearch(i,"Response $json / success info")

                removePlayerData()
                getIpAddressAndGoToGeneratePlayerData()
            }
            HttpURLConnection.HTTP_INTERNAL_ERROR -> { // Internal server error
                handleError(getString(R.string.text_internal_server_error))
            }
        }
    }

    /**
     * This function find the next step after player code generation or code checking
     */
    private fun findNextStep() {
        val data = JSONObject()
        val obj = prefs.socketOn

        if (!player.hasJoinRoom) {
            data.put("room", "room_${prefs.playerId}")
            data.put("player_code",  prefs.playerCode)
            Log.i("DATA TO JOIN", data.toString())
            Log.i("PLAYER_CODE", prefs.playerCode)
            socket.emit(Constant.EMIT_JOIN, data, Ack { args ->
                writeLog("Player join rom", "info")
                HyperLog.i(HLTAG,"Player join rom")
                pushLogsOnElasticSearch(i,"Player join rom")
                val joinRes = JSONObject()
                joinRes.put("socket_response", GsonBuilder().create().toJson(args))
                joinRes.put("channel", EMIT_MY_PING)
                joinRes.put("ping_data", "$data")
                Log.e(HLTAG, GsonBuilder().create().toJson(args))
                HyperLog.i(HLTAG,"Player join rom stack : $joinRes : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "Player join rom stack : $joinRes : ${HLTAG.toUpperCase()}")
                player.hasJoinRoom = true

                if (obj?.player_code != null && obj.playerid != null && obj.publickey != null && obj.status != null) {
                    data.put("player_code", obj.player_code)
                    data.put("playerid", obj.playerid)
                    data.put("publickey", obj.publickey)
                    data.put("status", StringUtils.capitalize(prefs.playerStatus?.toLowerCase()))
                    data.put("event", obj.publickey)
                    data.put("mode",  if (obj.mode != null) "Offline" else "Online")
                    socket.emit(EMIT_MY_PING, data, Ack { res ->
                        val jsonObj = JSONObject()
                        jsonObj.put("socket_response", GsonBuilder().create().toJson(res))
                        jsonObj.put("channel", EMIT_MY_PING)
                        jsonObj.put("ping_data", "$data")
                        HyperLog.i(HLTAG,"Player ping response stack : $jsonObj : ${HLTAG.toUpperCase()}")
                        pushLogsOnElasticSearch(i, "Player ping response stack : $jsonObj : ${HLTAG.toUpperCase()}")
                        // Log
                        Log.i(SocketHelper.TAG, "Player emit my ping success")
                        writeLog("Player ping response", "info")
                    })
                }
            })
        } else {
            data.put("player_code", prefs.playerCode)
            data.put("playerid", prefs.playerId)
            data.put("publickey", player.publickey)
            data.put("status", StringUtils.capitalize(prefs.playerStatus?.toLowerCase()))
            data.put("event", player.publickey)
            data.put("mode",  if ( prefs.playerMode != null) "Offline" else "Online")
            socket.emit(EMIT_MY_PING, data, Ack { res ->
                val jsonObj = JSONObject()
                jsonObj.put("socket_response", GsonBuilder().create().toJson(res))
                jsonObj.put("channel", EMIT_MY_PING)
                jsonObj.put("ping_data", "$data")
                HyperLog.i(HLTAG,"Player ping response stack : $jsonObj : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "Player ping response stack : $jsonObj : ${HLTAG.toUpperCase()}")
                // Log
                Log.i(SocketHelper.TAG, "Player emit my ping success")
                writeLog("Player ping response", "info")
            })
        }

        SocketHelper.listen(this, CHANNEL)
        updateView()
    }

    /**
     * Check preferences data and player status to determine which process to do
     * Also check if campaign from version 2 and determine which queue to start
     */
    private fun updateView() {
        if (prefs.prefDataAreNotEmpty() && prefs.playerStatus == Player.State.PLAYING.name) {
            when (prefs.campaign?.from_v2) {
                true -> { // play_now process (from_v2)
                    pushLogsOnElasticSearch(i,"Start process for rightplayer web(dashboard) new version. : ${HLTAG.toUpperCase()}")
                    playNowCampaignQueue[Timestamp(System.currentTimeMillis()).time] = prefs.campaign
                    if(!playNowQueueIsRunning) {
                        playNowStartQueue()
                    }
                }
                false -> { // Default process
                    pushLogsOnElasticSearch(i,"Start process for rightplayer web(dashboard) old version. : ${HLTAG.toUpperCase()}")
                    campaignQueue[Timestamp(System.currentTimeMillis()).time] = prefs.campaign
                    if(!queueIsRunning) {
                        startQueue()
                    }
                }
            }
        } else{
            removeAllFragment()
            loadFragment(supportFragmentManager, PlayerStateFragment(), PlayerStateFragment.HLTAG, false)
        }
    }

    /**
     * Start queue for old version of campaigns
     */
    private fun startQueue() {
        pushLogsOnElasticSearch(i,"Start default(rightplayer web old version) queue process. : ${HLTAG.toUpperCase()}")
        if (campaignQueue.isNotEmpty()) {
            if (!queueIsRunning) {
                queueIsRunning = true
            }
            val campaignKey = campaignQueue.entries.first().key
            val campaignValue = campaignQueue.entries.first().value

            Log.e("LAUNCH CAMPAIGN", "${campaignValue?.campaign_name}")

            if (campaignValue != null) {
                prefs.campaign = campaignValue
                removeAllFragment()
                loadFragment(supportFragmentManager, CampaignFragment.newInstance(campaignKey, campaignValue), CampaignFragment.HLTAG, false)
            } else {
                pushLogsOnElasticSearch(i,"Remove $campaignKey from default queue. : ${HLTAG.toUpperCase()}")
                campaignQueue.remove(campaignKey)
                startQueue()
            }
        } else {
            queueIsRunning = false

            val data = JSONObject()
            data.put("player_code",  prefs.playerCode)
            data.put("playerid", prefs.playerId)
            data.put("publickey",  prefs.socketOn?.publickey)
            data.put("campaign", prefs.campaign?.campaign_name)
            socket.emit(EMIT_PLAYING, data)
            Log.e("CAMPAIGN QUEUE", "Queue finish to run")
            pushLogsOnElasticSearch(i,"Default queue process finish running and we emit on channel $EMIT_PLAYING. : ${HLTAG.toUpperCase()}")
        }
    }

    /**
     * This function handle error when api failed
     * @param errorText
     */
    private fun handleError(errorText: String) {
        if (!prefs.prefDataAreNotEmpty()) {
            updateView()
        } else {
            tvError?.text = errorText
            if (tvError?.visibility != View.VISIBLE) {
                tvError?.visibility = View.VISIBLE
            }

            if (deviceHasInternetAccess()) {
                if (tvError?.visibility != View.GONE) {
                    tvError?.visibility = View.GONE
                }

                removeAllFragment()
                loadFragment(supportFragmentManager, SplashScreenFragment(), SplashScreenFragment.HLTAG, false)
            } else{
                // Call the function again after 3000ms to check internet and to try to continue process
                Handler().postDelayed({
                    handleError(getString(R.string.text_error_connection))
                }, 3000)
            }
        }
    }

    /**
     * When receive event on new channel(play_now), start process for this channel
     */
    private fun startPlayNowProcess(args: Any, channel: String) {
        Log.e(SocketHelper.TAG, "Receive event on channel $channel")
        Log.e(HLTAG, "PrincipalActivity socket channel $channel action PLAYING.")
        Log.e(SocketHelper.TAG, "Receive on $channel")
        writeLog("Player received action to play campaign ${prefs.socketOn?.status?.toLowerCase()}", "info")
        HyperLog.i(HLTAG, "Player's status change to ${prefs.socketOn?.status?.toLowerCase()}. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i,"Player's status change to ${prefs.socketOn?.status?.toLowerCase()}. : ${HLTAG.toUpperCase()}")

        val campaign = fromJsonToCampaign(GsonBuilder().create().toJson(args))
        playNowCampaignQueue[Timestamp(System.currentTimeMillis()).time] = campaign

        if (!playNowQueueIsRunning) {
            playNowStartQueue()
        }
    }

    /**
     * Start queue for rightplayer web(dashboard) new version channel
     */
    private fun playNowStartQueue() {
        pushLogsOnElasticSearch(i,"Start \"play_now\"(rightplayer web new version) queue process. : ${HLTAG.toUpperCase()}")
        if (playNowCampaignQueue.isNotEmpty()) {
            if (!playNowQueueIsRunning) {
                playNowQueueIsRunning = true
            }
            val campaignKey = playNowCampaignQueue.entries.first().key
            val campaignValue = playNowCampaignQueue.entries.first().value

            Log.e("LAUNCH CAMPAIGN", "${campaignValue?.campaign_name}")

            if (campaignValue != null) {
                prefs.campaign = campaignValue
                removeAllFragment()
                loadFragment(supportFragmentManager, CampaignFragment.newInstance(campaignKey, campaignValue), CampaignFragment.HLTAG, false)
            } else {
                pushLogsOnElasticSearch(i,"Remove $campaignKey from \"play_now\" queue. : ${HLTAG.toUpperCase()}")
                playNowCampaignQueue.remove(campaignKey)
                startQueue()
            }
        } else {
            playNowQueueIsRunning = false

            val data = JSONObject()
            data.put("player_code",  prefs.playerCode)
            data.put("playerid", prefs.playerId)
            data.put("publickey",  prefs.socketOn?.publickey)
            data.put("campaign", prefs.campaign?.campaign_name)
            socket.emit(EMIT_PLAYING, data)
            Log.e("PLAY NOW CAMPAIGN QUEUE", "Queue finish to run")
            pushLogsOnElasticSearch(i,"\"play_now\" queue process finish running and we emit on channel $EMIT_PLAYING. : ${HLTAG.toUpperCase()}")
        }
    }

    companion object {
        var CHANNEL = arrayListOf(
                ON_GET_IP_SERVER, ON_RELOAD, ON_RELAUNCH, ON_SEND_COMMAND,
                ON_UPDATE_PLAYER, ON_MOVETO, ON_CHANGE_MODE, ON_REFRESH,
                ON_SCHEDULE_INFO, ON_SCHEDULE_DELETE_INFO, ON_SCHEDULE_UPDATE_INFO,
                ON_PLAY_NOW)
    }
}
