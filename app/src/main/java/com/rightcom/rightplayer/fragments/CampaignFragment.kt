package com.rightcom.rightplayer.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.webkit.*
import com.google.gson.GsonBuilder
import com.hypertrack.hyperlog.HyperLog
import com.rightcom.rightplayer.R
import com.rightcom.rightplayer.helpers.DownloadHelper
import com.rightcom.rightplayer.helpers.FileManagerHelper
import com.rightcom.rightplayer.helpers.UnzipHelper
import com.rightcom.rightplayer.models.Models
import com.rightcom.rightplayer.player
import com.rightcom.rightplayer.prefs
import com.rightcom.rightplayer.socket
import com.rightcom.rightplayer.tools.Constant
import com.rightcom.rightplayer.tools.Constant.EMIT_PLAYING
import com.rightcom.rightplayer.tools.Constant.FILE_NAME
import com.rightcom.rightplayer.tools.Constant.FILE_URL
import com.rightcom.rightplayer.tools.Constant.LOGO_VIDEO
import com.rightcom.rightplayer.tools.Constant.MODE_OFFLINE
import com.rightcom.rightplayer.tools.Constant.MODE_ONLINE
import com.rightcom.rightplayer.tools.Constant.VIDEO_URL_1
import com.rightcom.rightplayer.tools.Constant.VIDEO_URL_10
import com.rightcom.rightplayer.tools.Constant.VIDEO_URL_11
import com.rightcom.rightplayer.tools.Constant.VIDEO_URL_12
import com.rightcom.rightplayer.tools.Constant.VIDEO_URL_13
import com.rightcom.rightplayer.tools.Constant.VIDEO_URL_14
import com.rightcom.rightplayer.tools.Constant.VIDEO_URL_2
import com.rightcom.rightplayer.tools.Constant.VIDEO_URL_3
import com.rightcom.rightplayer.tools.Constant.VIDEO_URL_4
import com.rightcom.rightplayer.tools.Constant.VIDEO_URL_5
import com.rightcom.rightplayer.tools.Constant.VIDEO_URL_6
import com.rightcom.rightplayer.tools.Constant.VIDEO_URL_7
import com.rightcom.rightplayer.tools.Constant.VIDEO_URL_8
import com.rightcom.rightplayer.tools.Constant.VIDEO_URL_9
import com.rightcom.rightplayer.tools.Constant.i
import com.rightcom.rightplayer.tools.TypefaceManager
import com.rightcom.rightplayer.tools.Utils
import com.rightcom.rightplayer.tools.Utils._dirChecker
import com.rightcom.rightplayer.tools.Utils.campaignDownloadedSettingsFileExist
import com.rightcom.rightplayer.tools.Utils.campaignsStorage
import com.rightcom.rightplayer.tools.Utils.customStorage
import com.rightcom.rightplayer.tools.Utils.downloadsStorage
import com.rightcom.rightplayer.tools.Utils.jsonToMap
import com.rightcom.rightplayer.tools.Utils.pushLogsOnElasticSearch
import com.rightcom.rightplayer.tools.Utils.templateStorage
import kotlinx.android.synthetic.main.fragment_campaign.view.*
import org.json.JSONObject
import java.io.File
import java.io.FileWriter
import java.util.regex.Pattern

private const val ARG_CAMPAIGN_KEY = "campaignKey"
private const val ARG_CAMPAIGN_VALUE = "campaignValue"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [CampaignFragment.CampaignFragmentInteraction] interface
 * to handle interaction events.
 * Use the [CampaignFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class CampaignFragment : Fragment(),
        DownloadHelper.DownloaderCallback,
        UnzipHelper.UnzipCallback,
        FileManagerHelper.FileManagerCallback {

    private lateinit var root: View
    private var map = HashMap<String, String>()
    private var mapToDownload = HashMap<String, String>()
    private var countDownloadFiles: Int = 0
    private var count = 0

    /* Play_Now process properties */
    private var pnMap = HashMap<String, String>()
    private var pnMapToDownload = HashMap<String, String>()
    private var pnCountDownloadFiles: Int = 0
    private var pnCount = 0

    private var campaignKey: Long? = null
    private var campaignValue: Models.Campaign? = null
    private var mListener: CampaignFragmentInteraction? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            campaignKey = it.getLong(ARG_CAMPAIGN_KEY)
            campaignValue = it.getParcelable<Models.Campaign>(ARG_CAMPAIGN_VALUE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        root = inflater.inflate(R.layout.fragment_campaign, container, false)
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON) // To keep screen alive

        root.pb_message?.typeface = TypefaceManager.regularTypeFace(context!!)
        root.pb_error_message?.typeface = TypefaceManager.regularTypeFace(context!!)

        return root
    }

    override fun onStart() {
        super.onStart()
        // Check if template already exist
        val campaign = campaignValue

        if (campaign?.fromTemplate!!) {
            val campaignExist = Utils.campaignExist(campaign?.template!!)
            if (campaignExist.inCampaign && campaignExist.inTemplate) {
                Log.i("EXIST_IN_CAMPAIGN", campaignExist.path)
                HyperLog.i("EXIST_IN_CAMPAIGN", "${campaignExist.path} : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "${campaignExist.path} : $HLTAG")
                prefs.campaignPath = campaignExist.path

                checkPlayersModeAndStartProcess()
            } else if (!campaignExist.inCampaign && campaignExist.inTemplate) {
                Log.i("EXIST_IN_TEMPLATE", campaignExist.path)
                HyperLog.i("EXIST_IN_TEMPLATE", "${campaignExist.path} : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "${campaignExist.path} : $HLTAG")
                UnzipHelper.requestUnzip(File(campaignExist.path), this@CampaignFragment)
            } else if (!campaignExist.inCampaign && !campaignExist.inTemplate) {
                Log.i("NOT_EXIST_IN", "Download the campaign")
                HyperLog.i("NOT_EXIST_IN", "Download the campaign. : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "Download the campaign. : $HLTAG")
                DownloadHelper.download(campaign.template!!, this@CampaignFragment, templateStorage, null, null, false)
            }
        } else if (!campaign.fromTemplate) {
            htmlCampaignProcess(campaign)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        root.webView?.removeAllViews()
        root.webView?.clearHistory()
        root.webView?.clearCache(true)
        root.webView?.onPause()
        root.webView?.removeAllViews()
        root.webView?.destroyDrawingCache()
        root.webView?.destroy()

        HyperLog.i(HLTAG, "Destroy campaign fragment view objects. : ${HLTAG.toUpperCase()}")
        Log.e(HLTAG, "CampaignFragment onDestroyView")
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            if (context is CampaignFragmentInteraction) {
                mListener = context
            }
        } catch (e: Exception) {
            pushLogsOnElasticSearch(Constant.e, "CampaignFragment onAttach exception : ${e.message}. : ${HLTAG.toUpperCase()}")
        }

        /*if (context is CampaignFragmentInteraction) {
            mListener = context
        } else {
            throw RuntimeException("${context.toString()} must implement CampaignFragmentInteraction") as Throwable
        }*/
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
        pushLogsOnElasticSearch(i, "CampaignFragment detach from activity. : ${HLTAG.toUpperCase()}")
    }

    @SuppressLint("SetTextI18n")
    override fun onDownloadStart(fileName: String?) {
        if (fileName != null) {
            count++
//            root.pb_message?.text = getString(R.string.text_download_progress_start) + "  $count/$countDownloadFiles"
            root.pb_message?.text = getString(R.string.text_download_progress_start) + "..."
        } else {
            root.pb_message?.text = getString(R.string.text_download_campaign_progress_start)
        }
        showProgressBar()
        HyperLog.i(HLTAG, "Start downloading ${fileName ?: "campaign"}. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i, "Start downloading ${fileName ?: "campaign"}. : ${HLTAG.toUpperCase()}")
    }

    override fun onDownload(hasBeenDownload: Boolean, filePath: String, key: String?, value: String?) {
        // Download success
        if (hasBeenDownload) {
            if (filePath.contains(".zip")) {
                HyperLog.i(HLTAG, "Campaign successfully downloaded. : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "Campaign successfully downloaded. : ${HLTAG.toUpperCase()}")
                // Request file unzipping when download finish
                Thread.sleep(500)
                UnzipHelper.requestUnzip(File(filePath), this@CampaignFragment)
            } else {
                when (campaignValue?.from_v2) {
                    true -> { // from_v2 process
                        HyperLog.i(HLTAG, "Successfully download -> $value. : ${HLTAG.toUpperCase()}")
                        pushLogsOnElasticSearch(i, "Successfully download -> $value. : ${HLTAG.toUpperCase()}")

                        Log.i("FROM_V2_DOWNLOADED", "Successfully download -> $value. : ${HLTAG.toUpperCase()}")
                        Log.i("FROM_V2_FILE PATH", filePath)
                        pnMap[key?.substring(0, key.lastIndexOf('|'))!!] = "file://$filePath"
                        pnMapToDownload.remove(key)
                        playNowDownloadSettingFile()
                    }
                    false -> { // Default process
                        HyperLog.i(HLTAG, "Successfully download -> $value. : ${HLTAG.toUpperCase()}")
                        pushLogsOnElasticSearch(i, "Successfully download -> $value. : ${HLTAG.toUpperCase()}")

                        Log.i("DOWNLOADED", "Successfully download -> $value. : ${HLTAG.toUpperCase()}")
                        Log.i("FILE PATH", filePath)
                        map[key!!] = "file://$filePath"
                        mapToDownload.remove(key)
                        downloadSettingFile()
                    }
                }
            }
        } else {
            // Download failed
            mListener?.finishToLoadCampaign(campaignKey!!, campaignValue?.campaign_name!!, true)
            HyperLog.i(HLTAG, "Download process failure. : ${HLTAG.toUpperCase()}")
            pushLogsOnElasticSearch(i, "Download process failure. : ${HLTAG.toUpperCase()}")
            root.pb_message?.text = getString(R.string.text_download_progress_failed) // Update download progress bar message
            root.pb_error_message?.text = getString(R.string.text_download_progress_failed_action) // Update download progress bar message
            if (root.pb_error_message?.visibility == View.GONE)
                root.pb_error_message?.visibility = View.VISIBLE
            root.pb_download?.visibility = View.GONE
        }
    }

    override fun onDownloadProgressUpdated(progress: Int) {
        root.pb_download.isIndeterminate = false
        root.pb_download.max = 100
        root.pb_download.progress = progress
    }

    override fun onUnzipStart() {
        HyperLog.i(HLTAG, "Start unzipping downloaded file. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i, "Start unzipping downloaded file. : ${HLTAG.toUpperCase()}")

        root.pb_message?.text = getString(R.string.text_unzip_progress_start)
        showProgressBar()
    }

    override fun onUnzipProgressUpdated(progress: Int) {
        root.pb_download.isIndeterminate = false
        root.pb_download.max = 100
        root.pb_download.progress = progress
    }

    override fun onUnzip(hasBeenUnzip: Boolean, folderPath: String) {
        if (hasBeenUnzip) {
            Utils.writeLog("Player received action to play campaign ${prefs.socketOn?.status?.toLowerCase()}", "info")
            HyperLog.i(HLTAG, "Player received action to play campaign ${prefs.socketOn?.status?.toUpperCase()}. : ${HLTAG.toUpperCase()}")
            pushLogsOnElasticSearch(i, "Player received action to play campaign ${prefs.socketOn?.status?.toUpperCase()}. : ${HLTAG.toUpperCase()}")
            prefs.campaignPath = folderPath
            hideProgressBar()
            checkPlayersModeAndStartProcess()
        } else {
            mListener?.finishToLoadCampaign(campaignKey!!, campaignValue?.campaign_name!!, true)
            root.pb_message?.text = getString(R.string.text_unzip_process_failed)
            root.pb_download?.visibility = View.GONE
        }
    }

    override fun onWriteInSetting(hasWriteInSetting: Boolean) {
        if (hasWriteInSetting) {
            val pData = JSONObject()
            pData.put("player_code", prefs.playerCode)
            pData.put("playerid", prefs.playerId)
            pData.put("publickey", player.publickey)
            pData.put("campaign", campaignValue?.campaign_name)
            socket.emit(EMIT_PLAYING, pData)
            Utils.writeLog("Player is playing ${campaignValue?.campaign_name}", "info")
            HyperLog.i(HLTAG, "Player is playing ${campaignValue?.campaign_name}. : ${HLTAG.toUpperCase()}")
            pushLogsOnElasticSearch(i, "Player is playing ${campaignValue?.campaign_name}. : ${HLTAG.toUpperCase()}")

            initWebView("${prefs.campaignPath}/index.html")
        }
    }

    /**
     * Initialize the webView content
     */
    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView(filePath: String) {
        hideProgressBar()
        HyperLog.i(HLTAG, "Init web view => file://$filePath : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i, "Init web view => file://$filePath : ${HLTAG.toUpperCase()}")

        root.webView?.loadUrl("file://$filePath")
        root.webView?.settings?.javaScriptEnabled = true
        root.webView?.settings?.pluginState = WebSettings.PluginState.ON
        root.webView?.settings?.allowFileAccess = true
        root.webView?.settings?.domStorageEnabled = true
        root.webView?.settings?.setAppCacheEnabled(true)
        root.webView?.resumeTimers()
        root.webView?.webViewClient = object: WebViewClient() {
            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                view?.loadUrl(request?.url.toString())
                HyperLog.i(HLTAG, "Override loading url in webView. : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "Override loading url in webView. : ${HLTAG.toUpperCase()}")
                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                root.loader.visibility = View.GONE
                HyperLog.i(HLTAG, "WebView page started. : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "WebView page started. : ${HLTAG.toUpperCase()}")
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                hideProgressDialog()
                root.loader.visibility = View.GONE
                root.webView.visibility = View.VISIBLE
                HyperLog.i(HLTAG, "WebView page finished loading. : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "WebView page finished loading. : ${HLTAG.toUpperCase()}")
                mListener?.finishToLoadCampaign(campaignKey!!, campaignValue?.campaign_name!!, false)
                super.onPageFinished(view, url)
            }
        }
        root.webView?.webChromeClient = WebChromeClient()
        root.webView?.settings?.mediaPlaybackRequiresUserGesture = false
        root.webView?.settings?.defaultTextEncodingName = "utf-8"
        root.webView?.clearCache(true)
        root.webView?.setOnTouchListener { _, _ -> true }
        root.webView?.setOnTouchListener {  _, _ -> true
        }
        WebView.setWebContentsDebuggingEnabled(true)
    }

    /**
     * Initialize the download progress bar
     */
    private fun initDownloadProgress() {
        root.pb_download.isIndeterminate = true
    }

    /**
     * Show loader on webView start loading page
     */
    private fun showProgressDialog() {
        HyperLog.i(HLTAG, "Show download progress dialog. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i, "Show download progress dialog. : ${HLTAG.toUpperCase()}")
        root.loader.visibility = View.VISIBLE
    }

    /**
     * Show loader on webView finish loading page
     */
    private fun hideProgressDialog() {
        HyperLog.i(HLTAG, "Hide download progress dialog. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i, "Hide download progress dialog. : ${HLTAG.toUpperCase()}")
        root.loader.visibility = View.GONE
    }

    /**
     * Show download progress bar
     */
    private fun showProgressBar() {
        HyperLog.i(HLTAG, "Show download progress bar. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i, "Show download progress bar. : ${HLTAG.toUpperCase()}")
        root.pb_message.visibility = View.VISIBLE
        root.pb_download.visibility = View.VISIBLE
        hideProgressDialog()
    }

    /**
     * Hide download progress bar
     */
    private fun hideProgressBar() {
        HyperLog.i(HLTAG, "Hide download progress bar. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i, "Hide download progress bar. : ${HLTAG.toUpperCase()}")
        root.pb_message.visibility = View.GONE // Hide download progress bar message
        root.pb_download.visibility = View.GONE // Hide download progress bar
        showProgressDialog() // Show loader
    }

    /**
     * Check the player's mode and determine the process to do
     */
    private fun checkPlayersModeAndStartProcess() {
        when(prefs.playerMode!!.toUpperCase()) {
            MODE_ONLINE.toUpperCase() -> {
                when(campaignValue?.from_v2) {
                    true ->{ playNowOnlineProcess() }
                    false -> { onlineProcess() }
                }
            }
            MODE_OFFLINE.toUpperCase() -> {
                when(campaignValue?.from_v2) {
                    true -> { // Receive event on channel play_now action playing
                        Log.e("FROM_V2", "Campaign publish from v2 in offline mode.")
                        // Initialize play_now process map
                        pnMap = jsonToMap(campaignValue?.rplayer!!)
                        // Make a pause
                        Thread.sleep(500)
                        playNowBeginFileDownload()
                    }
                    false -> { // Receive event on channel moveto action playing
                        // Convert campaign settings to map
                        map = jsonToMap(campaignValue?.rplayer!!)
                        Thread.sleep(500)
                        beginFileDownload()
                    }
                }
            }
        }
    }

    /**
     * Process of the player in online mode
     */
    private fun onlineProcess() {
        HyperLog.i(HLTAG, "Start player online mode process. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i, "Start player online mode process. : ${HLTAG.toUpperCase()}")
        FileManagerHelper.requestWriteInSetting(this, prefs.campaignPath!!, campaignValue?.rplayer!!)
    }

    /**
     * Process of the player's new version in online mode
     */
    private fun playNowOnlineProcess() {
        HyperLog.i(HLTAG, "Start player online mode process in version 2. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i, "Start player online mode process in version 2. : ${HLTAG.toUpperCase()}")
        playNowRewriteCampaignRPlayer()
    }

    /**
     * Rewrite campaign's rplayer value before write settings.js file
     */
    private fun playNowRewriteCampaignRPlayer() {
        val playNowMap = jsonToMap(campaignValue?.rplayer!!)
        val obj = JSONObject(GsonBuilder().create().toJson(campaignValue?.rplayer))
        var rewriteValue = ""
        for (k in obj.keys()) {
            if (k.startsWith("video", true)) {
                val nameValuePairs = obj.getJSONObject(k).optJSONObject("nameValuePairs")
                rewriteValue = nameValuePairs?.getString(FILE_URL)?.replace("as_attachment=yes", "as_attachment=no") ?: obj.getJSONObject(k).getString(FILE_URL).replace("as_attachment=yes", "as_attachment=no")
                playNowMap[k] = rewriteValue
            } else if (k.startsWith("logo", true)) {
                val nameValuePairs = obj.getJSONObject(k).optJSONObject("nameValuePairs")
                rewriteValue = nameValuePairs?.getString(FILE_URL)?.replace("as_attachment=yes", "as_attachment=no") ?: obj.getJSONObject(k).getString(FILE_URL).replace("as_attachment=yes", "as_attachment=no")
                playNowMap[k] = rewriteValue
            } else if (k.startsWith("image", true) || k.startsWith("img", true)) {
                val nameValuePairs = obj.getJSONObject(k).optJSONObject("nameValuePairs")
                rewriteValue = nameValuePairs?.getString(FILE_URL)?.replace("as_attachment=yes", "as_attachment=no") ?: obj.getJSONObject(k).getString(FILE_URL).replace("as_attachment=yes", "as_attachment=no")
                playNowMap[k] = rewriteValue
            }
        }
        Log.e(HLTAG, "MAP = $playNowMap")
        campaignValue?.rplayer = playNowMap
        pushLogsOnElasticSearch(i, "Rewrite campaign's rplayer json content : $playNowMap. : ${HLTAG.toUpperCase()}")
        Log.e(HLTAG, "Campaign path = ${prefs.campaignPath}")
        Log.e(HLTAG, "Campaign rplayer = ${campaignValue?.rplayer}")
        FileManagerHelper.requestWriteInSetting(this, prefs.campaignPath!!, campaignValue?.rplayer!!)
    }

    /**
     * Process of the player for html campaign
     */
    private fun htmlCampaignProcess(campaign: Models.Campaign) {
        showProgressDialog()
        _dirChecker(customStorage.absolutePath) // Check if html directory already exist and create it if not
        val indexFile = File(customStorage.absolutePath + "/index.html")

        try {
            // Check if index file exist and delete content else create index file
            if (indexFile.exists()) {
                FileManagerHelper.deleteFileContent(indexFile.absolutePath)
            } else {
                indexFile.createNewFile()
            }

            // Write campaign html in index file
            FileWriter(indexFile).use { file ->
                file.write(campaign.editor_code?.html)
            }

            pushLogsOnElasticSearch(i, "HTML campaign process success. : ${HLTAG.toUpperCase()}")
            initWebView(indexFile.absolutePath) // Load index file in web view
        } catch (e: Exception) {
            hideProgressBar()
            hideProgressDialog()
            root.pb_message.text = getString(R.string.text_loading_error)
            root.pb_error_message.text = getString(R.string.text_refresh_player_from_dashboard)
            pushLogsOnElasticSearch(Constant.e, "HTML campaign process failed : ${e.message}. : ${HLTAG.toUpperCase()}")
        }
    }

    /**
     * Begin process to download campaign files if player mode is offline
     * This function was created for old version of rightplayer web(dashboard)
     */
    private fun beginFileDownload() {
        for (key in map.keys) {
            if (validURL(map[key]!!) && !alreadyExist(map[key]!!)){
                mapToDownload[key] = map[key]!!
            }
        }
        countDownloadFiles = mapToDownload.size
        pushLogsOnElasticSearch(i, "Prepare map of file to download (default process). : ${HLTAG.toUpperCase()}")
        downloadSettingFile()
    }

    /**
     * Start file download process after checking if the file does not
     * already exist in device local storage
     * This function was created for old version of rightplayer web(dashboard)
     */
    private fun downloadSettingFile() {
        if (mapToDownload.size > 0) {
            val filePathKey = mapToDownload.entries.first().key
            val filePathValue = mapToDownload.entries.first().value

            val finalFilePath = filePathValue.replace(" ", "%20")
            val localPath = downloadsStorage.absolutePath + "/" + filePathValue.substring(filePathValue.lastIndexOf('/')+1)

            if (!campaignDownloadedSettingsFileExist(localPath)) {
                HyperLog.i(HLTAG, "Prepare download of -> $finalFilePath : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "Prepare download of -> $finalFilePath : ${HLTAG.toUpperCase()}")
                Thread.sleep(1000)
                DownloadHelper.download(finalFilePath, this@CampaignFragment, downloadsStorage, filePathKey, finalFilePath, false)
            } else {
                map[filePathKey] = "file://$localPath"
                mapToDownload.remove(filePathKey)
                downloadSettingFile()
            }
        } else {
            val campaign = campaignValue
            campaign?.rplayer = map
            prefs.campaign = campaign
            FileManagerHelper.requestWriteInSetting(this, prefs.campaignPath!!, campaignValue?.rplayer!!)
        }
    }

    /**
     * Begin process to download campaign files if player mode is offline
     * This function was created for new version of rightplayer web(dashboard)
     * and include CDN campaign management
     */
    private fun playNowBeginFileDownload() {
        val obj = JSONObject(GsonBuilder().create().toJson(campaignValue?.rplayer))
        var fileurl = ""
        var fileName = ""
        for (k in obj.keys()) {
            if (k.startsWith("video", true)) {
                val nameValuePairs = obj.getJSONObject(k).optJSONObject("nameValuePairs")
                fileName = if (nameValuePairs != null) {
                    nameValuePairs.getString(FILE_NAME)
                } else {
                    obj.getJSONObject(k).getString(FILE_NAME)
                }
                fileurl = if (nameValuePairs != null) {
                    nameValuePairs.getString(FILE_URL)
                } else {
                    obj.getJSONObject(k).getString(FILE_URL)
                }
                if (!playNowAlreadyExist(fileurl)) {
                    pnMapToDownload["$k|$fileName"] = fileurl
                }
            } else if (k.startsWith("logo", true)) {
                val nameValuePairs = obj.getJSONObject(k).optJSONObject("nameValuePairs")
                fileName = if (nameValuePairs != null) {
                    nameValuePairs.getString(FILE_NAME)
                } else {
                    obj.getJSONObject(k).getString(FILE_NAME)
                }
                fileurl = if (nameValuePairs != null) {
                    nameValuePairs.getString(FILE_URL)
                } else {
                    obj.getJSONObject(k).getString(FILE_URL)
                }
                if (!playNowAlreadyExist(fileurl)) {
                    pnMapToDownload["$k|$fileName"] = fileurl
                }
            } else if (k.startsWith("image", true) || k.startsWith("img", true)) {
                val nameValuePairs = obj.getJSONObject(k).optJSONObject("nameValuePairs")
                fileName = if (nameValuePairs != null) {
                    nameValuePairs.getString(FILE_NAME)
                } else {
                    obj.getJSONObject(k).getString(FILE_NAME)
                }
                fileurl = if (nameValuePairs != null) {
                    nameValuePairs.getString(FILE_URL)
                } else {
                    obj.getJSONObject(k).getString(FILE_URL)
                }
                if (!playNowAlreadyExist(fileurl)) {
                    pnMapToDownload["$k|$fileName"] = fileurl
                }
            }
        }
        pnCountDownloadFiles = pnMapToDownload.size
        pushLogsOnElasticSearch(i, "Prepare map of file to download(new process). : ${HLTAG.toUpperCase()}")
        playNowDownloadSettingFile()
    }

    /**
     * Start file download process after checking if the file does not
     * already exist in device local storage
     * This function was created for new version of rightplayer web(dashboard)
     * and include CDN campaign management
     */
    private fun playNowDownloadSettingFile() {
        if (pnMapToDownload.size > 0) {
            val mKey = pnMapToDownload.entries.first().key
            val fileUrlValue = pnMapToDownload.entries.first().value
            val fileName = mKey.substring(mKey.lastIndexOf('|') + 1)

            val finalFileName = fileName.replace(" ", "%20")
            val localPath = downloadsStorage.absolutePath + "/" + finalFileName

            if (!campaignDownloadedSettingsFileExist(localPath)) {
                HyperLog.i(HLTAG, "Prepare download of -> $finalFileName : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "Prepare download of -> $finalFileName : ${HLTAG.toUpperCase()}")
                Thread.sleep(1000)
                DownloadHelper.download(fileUrlValue, this@CampaignFragment, downloadsStorage, mKey, fileUrlValue, true)
            } else {
                val pnMapKey = mKey.substring(0, mKey.lastIndexOf('|'))
                pnMap[pnMapKey] = "file://$localPath"
                pnMapToDownload.remove(mKey)
                playNowDownloadSettingFile()
            }
        } else {
            val campaign = campaignValue
            campaign?.rplayer = pnMap
            prefs.campaign = campaign
            FileManagerHelper.requestWriteInSetting(this, prefs.campaignPath!!, campaignValue?.rplayer!!)
        }
    }

    /**
     * Check a URL validity
     * @param url The URL to validate
     */
    private fun validURL(url: String): Boolean = Pattern.matches("^h(t){2}p(s)?(://).+\\.(jpeg|jpg|png|gif|mp4|mp3|avi)$", url)

    /**
     * Check if a valid URL exist in list of URLs
     * @param urls The list of URLs to browse
     */
    private fun urlExist(urls: MutableCollection<String>): Boolean {
        for (url in urls) {if (validURL(url)) return true}
        return  false
    }

    /**
     * Return a list of valid URLs
     */
    private fun getValidURLs(): Int {
        var count = 0
        for (value in map.values) {
            if (validURL(value))
                count++
        }
        return count
    }

    /**
     * Check if data already exist in the map
     * This function was created for old version of rightplayer web(dashboard) campaign management
     */
    private fun alreadyExist(value: String): Boolean {
        if (mapToDownload.isNotEmpty()) {
            for (key in mapToDownload.keys) {
                if (mapToDownload[key]!! == value) {
                    return true
                }
            }
            return false
        }
        return false
    }

    /**
     * Check if data already exist in the playnow map
     * This function was created for new version of rightplayer web(dashboard) campaign management
     */
    private fun playNowAlreadyExist(value: String): Boolean {
        if (pnMapToDownload.isNotEmpty()) {
            for (key in pnMapToDownload.keys) {
                if (pnMapToDownload[key]!! == value) {
                    return true
                }
            }
            return false
        }
        return false
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    interface CampaignFragmentInteraction {
        fun finishToLoadCampaign(campaignKey: Long, campaignName: String, loadingFailed: Boolean)
    }

    companion object {
        val HLTAG = CampaignFragment::class.java.simpleName

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param campaignKey
         * @param campaignValue
         * @return A new instance of fragment CampaignFragment.
         */
        @JvmStatic
        fun newInstance(campaignKey: Long, campaignValue: Parcelable) =
                CampaignFragment().apply {
                    arguments = Bundle().apply {
                        putLong(ARG_CAMPAIGN_KEY, campaignKey)
                        putParcelable(ARG_CAMPAIGN_VALUE, campaignValue)
                    }
                }

        val rplayer_video_key = arrayListOf(
                VIDEO_URL_1, VIDEO_URL_2, VIDEO_URL_3,
                VIDEO_URL_4, VIDEO_URL_5, VIDEO_URL_6,
                VIDEO_URL_7, VIDEO_URL_8, VIDEO_URL_9,
                VIDEO_URL_10, VIDEO_URL_11, VIDEO_URL_12,
                VIDEO_URL_13, VIDEO_URL_14)
    }
}