package com.rightcom.rightplayer.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.hypertrack.hyperlog.HyperLog
import com.rightcom.rightplayer.R
import com.rightcom.rightplayer.models.Player
import com.rightcom.rightplayer.prefs
import com.rightcom.rightplayer.socket
import com.rightcom.rightplayer.tools.Constant.EMIT_DEACTIVATE
import com.rightcom.rightplayer.tools.Constant.EMIT_READY
import com.rightcom.rightplayer.tools.Constant.WATCH_DEMO_URL
import com.rightcom.rightplayer.tools.Constant.e
import com.rightcom.rightplayer.tools.Constant.i
import com.rightcom.rightplayer.tools.TypefaceManager
import com.rightcom.rightplayer.tools.Utils
import com.rightcom.rightplayer.tools.Utils.generateCode
import com.rightcom.rightplayer.tools.Utils.insertHyphenInString
import com.rightcom.rightplayer.tools.Utils.pushLogsOnElasticSearch
import kotlinx.android.synthetic.main.fragment_player_state.*
import kotlinx.android.synthetic.main.fragment_player_state.view.*
import kotlinx.android.synthetic.main.layout_player_activation.view.*
import kotlinx.android.synthetic.main.layout_player_active.view.*
import kotlinx.android.synthetic.main.layout_player_error.view.*
import kotlinx.android.synthetic.main.layout_player_lock.view.*
import kotlinx.android.synthetic.main.layout_player_unactive.view.*
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PlayerStateFragment.OnPlayerStateFragmentListener] interface
 * to handle interaction events.
 */
class PlayerStateFragment : Fragment() {

    private var lockView: View? = null
    private var activeView: View? = null
    private var unactiveView: View? = null
    private var activationView: View? = null
    private var errorView: View? = null

    private var listener: OnPlayerStateFragmentListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_player_state, container, false)
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON) // To keep screen alive

        lockView = view.lyt_player_lock
        activeView = view.lyt_player_active
        unactiveView = view.lyt_player_unactive
        activationView = view.lyt_player_activation
        errorView = view.lyt_player_error

        Log.e("PLAYER STATE", "OnCreateView PLAYER STATE")

        initView(view)
        setTypefaceToView(view)
        handleListener(view)

        // Prepare video reader
        videoView?.setVideoURI(Uri.parse(WATCH_DEMO_URL))

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnPlayerStateFragmentListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnPlayerStateFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * Initialize the fragment views
     */
    private fun initView(view: View) {
        when (prefs.playerStatus) {
            Player.State.ACTIVE.name -> {
                HyperLog.i(HLTAG, "RightPlayer mobile's player activated. : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "RightPlayer mobile's player activated. : ${HLTAG.toUpperCase()}")
                lockView?.visibility = View.GONE
                activeView?.visibility = View.VISIBLE
                unactiveView?.visibility = View.GONE
                activationView?.visibility = View.GONE
                errorView?.visibility = View.GONE

                val readyData = JSONObject()
                readyData.put("player_code", prefs.playerCode)
                readyData.put("playerid", prefs.playerId)
                readyData.put("publickey", prefs.socketOn?.publickey)
                socket.emit(EMIT_READY, readyData)
            }
            Player.State.UNACTIVE.name -> {
                HyperLog.i(HLTAG, "RightPlayer mobile's player unactivated. : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "RightPlayer mobile's player unactivated. : ${HLTAG.toUpperCase()}")
                lockView?.visibility = View.GONE
                activeView?.visibility = View.GONE
                unactiveView?.visibility = View.VISIBLE
                activationView?.visibility = View.GONE
                errorView?.visibility = View.GONE

                val unactivatedData = JSONObject()
                unactivatedData.put("player_code", prefs.playerCode)
                unactivatedData.put("playerid", prefs.playerId)
                unactivatedData.put("publickey", prefs.socketOn?.publickey)
                socket.emit(EMIT_DEACTIVATE, unactivatedData)
            }
            Player.State.PENDING.name, Player.State.DELETED.name -> {
                HyperLog.i(HLTAG, "RightPlayer mobile's player pending. : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "RightPlayer mobile's player pending. : ${HLTAG.toUpperCase()}")
//                view.tvCode?.text = prefs.playerCode!!
                view.tvCode?.text = insertHyphenInString(prefs.playerCode!!)
                val qrCodeBitmap = generateCode(prefs.playerCode!!)
                if (qrCodeBitmap != null) {
                    view.imgQRCode?.setImageBitmap(qrCodeBitmap)
                }
                lockView?.visibility = View.GONE
                activeView?.visibility = View.GONE
                unactiveView?.visibility = View.GONE
                activationView?.visibility = View.VISIBLE
                errorView?.visibility = View.GONE
            }
            /*Player.State.DELETED.name -> {
                lockView?.visibility = View.VISIBLE
                activeView?.visibility = View.GONE
                unactiveView?.visibility = View.GONE
                activationView?.visibility = View.GONE
                errorView?.visibility = View.GONE
            }*/
            else -> {
                Utils.writeLog("RightPlayer reading local storage error.", "error")
                HyperLog.e(HLTAG, "Failed while trying to read local storage. : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(e, "Failed while trying to read local storage. : ${HLTAG.toUpperCase()}")
                lockView?.visibility = View.GONE
                activeView?.visibility = View.GONE
                unactiveView?.visibility = View.GONE
                activationView?.visibility = View.GONE
                errorView?.visibility = View.VISIBLE
            }
        }
    }

    /**
     * Set listener on watch a demo button
     */
    private fun handleListener(view: View) {
        view.mrlDemo.setOnClickListener {
            /*val builder: CustomTabsIntent.Builder = CustomTabsIntent.Builder()
            builder.setShowTitle(true)
            builder.setToolbarColor(activity?.resources?.getColor(R.color.colorBackground)!!)
            builder.enableUrlBarHiding()
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(activity?.applicationContext, Uri.parse(WATCH_DEMO_URL))*/

//            listener?.onClickWatchDemo()

//            with(videoView) {
//                visibility = View.VISIBLE
//                rlvPrincipal?.visibility = View.GONE
//                start()
//            }
        }

//        videoView?.setOnCompletionListener {
//            rlvPrincipal?.visibility = View.VISIBLE
//            videoView?.visibility = View.GONE
//        }

//        view.mrlDemo.setOnClickListener {
//            Toast.makeText(context, "This is a toast", Toast.LENGTH_SHORT).show()
//            try {
////                val proc = Runtime.getRuntime().exec("reboot")
////                proc.waitFor()
//                Log.v("TRY", "Reboot")
//                Runtime.getRuntime().exec("reboot -p")
//            } catch (ex: Throwable) {
//                ex.printStackTrace()
//                Log.v("ERROR", ex.message)
//                print("ERROR : ".toUpperCase() +ex.message)
//            }
//        }
    }

    /**
     * Set TypeFace on view's TextView
     */
    private fun setTypefaceToView(view: View) {
        if (context != null) {
            view.tv_right?.typeface = TypefaceManager.regularTypeFace(context!!)
            view.tv_player?.typeface = TypefaceManager.regularTypeFace(context!!)
            view.tv_copyright?.typeface = TypefaceManager.regularTypeFace(context!!)
            view.tvActiveTitle?.typeface = TypefaceManager.regularTypeFace(context!!)
            view.tvActiveContent?.typeface = TypefaceManager.regularTypeFace(context!!)
            view.tvUnactiveTitle?.typeface = TypefaceManager.regularTypeFace(context!!)
            view.tvUnactiveContent?.typeface = TypefaceManager.regularTypeFace(context!!)
            view.tvLockTitle?.typeface = TypefaceManager.regularTypeFace(context!!)
            view.tvLockContent?.typeface = TypefaceManager.regularTypeFace(context!!)
            view.tvActivationTitle?.typeface = TypefaceManager.regularTypeFace(context!!)
            view.tvActivationContent?.typeface = TypefaceManager.regularTypeFace(context!!)
            view.tvDemo?.typeface = TypefaceManager.regularTypeFace(context!!)
            view.tvDemoUrl?.typeface = TypefaceManager.regularTypeFace(context!!)
            view.tvCode?.typeface = TypefaceManager.regularTypeFace(context!!)
            view.tvErrorTitle?.typeface = TypefaceManager.regularTypeFace(context!!)
            view.tvErrorContent?.typeface = TypefaceManager.regularTypeFace(context!!)
            view.tvErrorContent?.typeface = TypefaceManager.regularTypeFace(context!!)
            view.tv_beta?.typeface = TypefaceManager.permanentMarkerTypeFace(context!!)
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    interface OnPlayerStateFragmentListener {
        fun onClickWatchDemo()
    }

    companion object {
        val HLTAG = PlayerStateFragment::class.java.simpleName
    }
}
