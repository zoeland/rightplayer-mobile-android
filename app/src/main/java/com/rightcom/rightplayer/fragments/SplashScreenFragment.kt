package com.rightcom.rightplayer.fragments

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hypertrack.hyperlog.HyperLog
import com.rightcom.rightplayer.*

import com.rightcom.rightplayer.tools.Constant
import com.rightcom.rightplayer.tools.TypefaceManager
import com.rightcom.rightplayer.tools.Utils
import kotlinx.android.synthetic.main.activity_splash_screen.*
import kotlinx.android.synthetic.main.activity_splash_screen.view.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SplashScreenFragment.OnSplashScreenFragmentInteraction] interface
 * to handle interaction events.
 */
class SplashScreenFragment : Fragment() {

    private var mListener: OnSplashScreenFragmentInteraction? = null
    private lateinit var root: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.activity_splash_screen, container, false)

        typefaceToView()
        return root
    }

    override fun onStart() {
        super.onStart()
        if (permissionGranted()) {
            root.rlvLoader?.visibility = View.VISIBLE
            root.rlvPermission?.visibility = View.GONE
            continueOpenProcess()
            HyperLog.i(HLTAG, "Permission Granted. : ${HLTAG.toUpperCase()}")
            Utils.pushLogsOnElasticSearch(Constant.i, "Permission Granted. : ${HLTAG.toUpperCase()}")
        } else{
            root.rlvLoader?.visibility = View.GONE
            root.rlvPermission?.visibility = View.VISIBLE
            HyperLog.i(HLTAG, "Asking for permission. : ${HLTAG.toUpperCase()}")
            Utils.pushLogsOnElasticSearch(Constant.i, "Asking for permission. : ${HLTAG.toUpperCase()}")
        }

        mrlEnablePermission?.setOnClickListener {
            requestPermissions(PERMISSION, REQUEST_PERMISSION_CODE) // Request permission
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == SplashScreenFragment.REQUEST_PERMISSION_CODE && grantResults.isNotEmpty()) {
            if (permissionGranted()) {
                HyperLog.i(HLTAG, "On Request Permission Result / Permission Granted. : ${HLTAG.toUpperCase()}")
                Utils.pushLogsOnElasticSearch(Constant.i, "On Request Permission Result / Permission Granted. : ${HLTAG.toUpperCase()}")
                rlvLoader?.visibility = View.VISIBLE
                rlvPermission?.visibility = View.GONE
                continueOpenProcess()
            } else {
                rlvLoader?.visibility = View.GONE
                rlvPermission?.visibility = View.VISIBLE
                HyperLog.i(HLTAG, "On Request Permission Result / Permission Not Granted. : ${HLTAG.toUpperCase()}")
                Utils.pushLogsOnElasticSearch(Constant.i, "On Request Permission Result / Permission Not Granted. : ${HLTAG.toUpperCase()}")
            }
        } else {
            rlvLoader?.visibility = View.GONE
            rlvPermission?.visibility = View.VISIBLE
            HyperLog.i(HLTAG, "On Request Permission Result / Request Permission. : ${HLTAG.toUpperCase()}")
            Utils.pushLogsOnElasticSearch(Constant.i, "On Request Permission Result / Request Permission. : ${HLTAG.toUpperCase()}")
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnSplashScreenFragmentInteraction) {
            mListener = context
        } else {
            throw Exception("You must implement OnSplashScreenFragmentInteraction")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * Set typeface on textView's view
     */
    private fun typefaceToView() {
        root.tvError?.typeface = TypefaceManager.regularTypeFace(appContext) // Change type face of the text
        root.tvEnableP?.typeface = TypefaceManager.regularTypeFace(appContext) // Change type face of the text
        root.tvPermissionDetail?.typeface = TypefaceManager.regularTypeFace(appContext) // Change type face of the text
        root.tvEnablePermission?.typeface = TypefaceManager.regularTypeFace(appContext) // Change type face of the text
    }

    /**
     * Loop on permission list and check if they are all granted
     * @return Boolean
     */
    private fun permissionGranted(): Boolean {
        SplashScreenFragment.PERMISSION.forEach { it ->
            if ((ContextCompat.checkSelfPermission(appContext, it) != PackageManager.PERMISSION_GRANTED)) {
                return false
            }
        }
        return true
    }

    /**
     * Check if player has data in shared preferences.
     * If it has it, we process to checking of the data else we request data generation for the player
     */
    private fun continueOpenProcess() {
        if (Utils.deviceHasInternetAccess()) {
            // Hide internet access error
            /*if (tvError?.visibility == View.VISIBLE) {
                tvError?.visibility = View.GONE
                HyperLog.i(HLTAG, "Hide internet access error text. : ${HLTAG.toUpperCase()}")
                Utils.pushLogsOnElasticSearch(Constant.i, "Hide internet access error text. : ${HLTAG.toUpperCase()}")
            }*/

            // Check player data to determine how to continue the process
            if ((prefs.playerId == null || prefs.playerId?.isEmpty()!!)
                    || (prefs.playerCode == null || prefs.playerCode?.isEmpty()!!)) {
                mListener?.requestGetIpAndCodeGeneration() // Request get device ip and code generation
            } else {
                mListener?.requestCodeChecking() // Request player data checking
            }
        }
        // Check if shared preferences content data and use local data to launch player
        else {
            mListener?.handleConnectionError()
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    interface OnSplashScreenFragmentInteraction {
        fun requestCodeChecking()
        fun requestGetIpAndCodeGeneration()
        fun handleConnectionError()
    }

    companion object {
        val HLTAG = SplashScreenFragment::class.java.simpleName
        private const val REQUEST_PERMISSION_CODE = 100
        private val PERMISSION =
                arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }
}
