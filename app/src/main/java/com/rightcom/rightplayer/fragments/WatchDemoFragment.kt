package com.rightcom.rightplayer.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import com.hypertrack.hyperlog.HyperLog
import com.rightcom.rightplayer.R
import com.rightcom.rightplayer.tools.Constant
import com.rightcom.rightplayer.tools.Constant.i
import com.rightcom.rightplayer.tools.Utils.pushLogsOnElasticSearch
import kotlinx.android.synthetic.main.fragment_watch_demo.view.*


class WatchDemoFragment : Fragment() {


    private var root: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_watch_demo, container, false)
        initWebView()
        handleListener()
        return root
    }


    /**
     * Initialize the webView
     */
    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        HyperLog.i(HLTAG, "Initializing fragment components. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i, "Initializing fragment components. : ${HLTAG.toUpperCase()}")
        root?.webView?.settings?.javaScriptEnabled = true
        root?.webView?.loadUrl(Constant.WATCH_DEMO_URL)
        root?.webView?.webViewClient = WebViewClient()
        root?.webView?.clearCache(true)
//        root.webView?.setOnTouchListener { v, event -> true }
    }

    /**
     * Handler of the webView back button onClick event
     */
    private fun handleListener() {
        root?.btnBack?.setOnClickListener {
            if (root?.webView?.canGoBack()!!)
                root!!.webView.goBack()
            else {
                this@WatchDemoFragment.activity?.onBackPressed()
//                shellExecutor("reboot now")
            }
        }
    }

    companion object {
        val HLTAG = WatchDemoFragment::class.java.simpleName
    }
}
