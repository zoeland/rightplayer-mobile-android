package com.rightcom.rightplayer.helpers

import android.os.AsyncTask
import android.util.Log
import com.hypertrack.hyperlog.HyperLog
import com.rightcom.rightplayer.tools.Constant
import com.rightcom.rightplayer.tools.Constant.i
import com.rightcom.rightplayer.tools.Utils._dirChecker
import com.rightcom.rightplayer.tools.Utils.fileName
import com.rightcom.rightplayer.tools.Utils.pushLogsOnElasticSearch
import com.rightcom.rightplayer.tools.Utils.sdCardPresent
import java.io.File
import java.io.FileOutputStream
import java.net.HttpURLConnection
import java.net.URL

/**
 * Created by Abdel-Faïçal on 17/07/2018.
 */
object DownloadHelper {

    private lateinit var storage: File
    private var downloadUrl = ""
    private var downloadFileName = ""
    private val TAG = DownloadHelper::class.java.simpleName.toUpperCase()
    private lateinit var listener: DownloaderCallback

    /**
     * This function prepare download data and execute download task
     * @param downloadUrl
     * @param listener
     */
    fun download(downloadUrl: String, listener: DownloaderCallback, destinationFolder: File, key: String?, value: String?, fromV2 :Boolean) {
        this.downloadUrl = downloadUrl
        this.storage = destinationFolder
//        this.downloadFileName = fileName(downloadUrl)
        when (fromV2) {
            true -> { this.downloadFileName = key?.substring(key.lastIndexOf('|') + 1)!! } // From_v2 or play_now process
            false -> { this.downloadFileName = fileName(downloadUrl) } // Default process
        }
        DownloadHelper.listener = listener

        if (sdCardPresent()) { // Check if sd card is mounted
            _dirChecker(storage.path) // Check if directory exist else create it
            DownloadingTask(key, value).execute()
        } else {
            Log.e(TAG, "Storage not mounted")
            HyperLog.i(TAG.toLowerCase(), "Storage not mounted. : ${TAG.toUpperCase()}")
            pushLogsOnElasticSearch(i, "Storage not mounted. : ${TAG.toUpperCase()}")
        }
    }

    /**
     * This class manage the download. The class take url of the file to download and download it.
     * When download are finish, the class emit on the call back the download data and notify that
     * download was been success. If the download failed, it notify the callback that the download failed
     */
    private class DownloadingTask(private val key: String?, private val value: String?) : AsyncTask<Void?, Int?, Void?>() {
        internal var outputFile: File? = null

        override fun onPreExecute() {
            Log.i(TAG, "Start download")
            HyperLog.i(TAG.toLowerCase(), "PreExecute download process. : ${TAG.toUpperCase()}")
//            pushLogsOnElasticSearch(i, "PreExecute download process. : ${TAG.toUpperCase()}")
            listener.onDownloadStart(value?.substring(value.lastIndexOf("/") + 1))
            super.onPreExecute()
        }

        override fun doInBackground(vararg arg0: Void?): Void? {
            HyperLog.i(TAG.toLowerCase(), "Downloading file in background. : ${TAG.toUpperCase()}")
//            pushLogsOnElasticSearch(i, "Downloading file in background. : ${TAG.toUpperCase()}")
            try {
                val url = URL(downloadUrl) // Create Download URl
                val c = url.openConnection() as HttpURLConnection // Open Url Connection
                c.requestMethod = "GET" // Set Request Method to "GET" since we are grtting data
                c.connect() // connect the URL Connection

                outputFile = File(storage, downloadFileName) // Create output file
                if (!outputFile!!.exists()) { // Create new file if not present
                    outputFile!!.createNewFile()
                }

                val fos = FileOutputStream(outputFile!!) // Get OutputStream for NewFile Location
                val inputStream = c.inputStream // Get InputStream for connection
                val buffer = ByteArray(2048) // Set buffer type
                var len1: Int // init length
                var total: Long = 0
                // this will be useful to display download percentage
                // might be -1: server did not report the length
                val fileLength = c.contentLength

                do {
                    len1 = inputStream.read(buffer)
                    if (len1 == -1)
                        break
                    total += len1
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((total * 100 / fileLength).toInt())

                    fos.write(buffer, 0, len1)

                } while (true)

                //Close all connection after doing task
                fos.close()
                inputStream.close()
                HyperLog.i(TAG.toLowerCase(), "File download finish. : ${TAG.toUpperCase()}")
//                pushLogsOnElasticSearch(i, "File download finish. : ${TAG.toUpperCase()}")
            } catch (e: Exception) {
//                Log.e(TAG, e.printStackTrace().toString())
                HyperLog.e(TAG.toLowerCase(), "${e.message} : ${TAG.toUpperCase()}")
                pushLogsOnElasticSearch(Constant.e, "${e.message} : ${TAG.toUpperCase()}")
                outputFile?.delete()
                outputFile = null
            }
            return null
        }

        override fun onProgressUpdate(vararg progress: Int?) {
            super.onProgressUpdate(*progress)
//            HyperLog.i(TAG, "Update download progress bar progress. : ${TAG.toUpperCase()}")
            listener.onDownloadProgressUpdated(progress[0]!!)
        }

        override fun onPostExecute(result: Void?) {
            HyperLog.i(TAG, "Finish download process. : ${TAG.toUpperCase()}")
            pushLogsOnElasticSearch(i, "Finish download process. : ${TAG.toUpperCase()}")
            // After task execution, we emit on the callback the status of task execution
            if (outputFile != null) {
                listener.onDownload(hasBeenDownload = true, filePath = outputFile!!.absolutePath, key = key, value = value) // Send call back
            } else {
                listener.onDownload(hasBeenDownload = false, filePath = "", key = key, value = value) // Send call back
            }
            super.onPostExecute(result)
        }
    }

    /**
     * This is the interface that the download requester the download
     * must implement this interface to receive the callback when download task finish
     */
    interface DownloaderCallback{
        /**
         * The callback
         * @param fileName : The name of the file to download
         */
        fun onDownloadStart(fileName: String?)

        /**
         * The callback
         * @param progress : The download rate
         */
        fun onDownloadProgressUpdated(progress: Int)

        /**
         * The callback
         * @param hasBeenDownload : explain if download has been success
         * @param filePath : path of the file which was download
         * @param key : The key to identify the file downloading
         * @param value : URL of the file to download
         */
        fun onDownload(hasBeenDownload: Boolean, filePath: String, key: String?, value: String?)
    }
}