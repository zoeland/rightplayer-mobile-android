package com.rightcom.rightplayer.helpers

import android.util.Log
import com.hypertrack.hyperlog.HyperLog
import com.rightcom.rightplayer.tools.Constant
import com.rightcom.rightplayer.tools.Constant.SETTINGS_PATH
import com.rightcom.rightplayer.tools.Constant.i
import com.rightcom.rightplayer.tools.Utils.objectMapper
import com.rightcom.rightplayer.tools.Utils.pushLogsOnElasticSearch
import org.json.JSONObject
import java.io.BufferedReader
import java.io.FileReader
import java.io.FileWriter


object FileManagerHelper {

    private val TAG = "FILE_MANAGER"
    private lateinit var listener: FileManagerCallback
    private var filePath: String? = null
    private var data: Any? = null

    /**
     *  This function prepare data to write in "setting.js" file
     *  @param listener The process execution context
     *  @param filePath The file path
     *  @param data The data to write inside the file
     */
    fun requestWriteInSetting(listener: FileManagerCallback, filePath: String, data: Any) {
        FileManagerHelper.listener = listener
        FileManagerHelper.filePath = filePath
        FileManagerHelper.data = data
        writeInSetting()
    }

    /**
     * Function used to write json data into setting js
     */
    private fun writeInSetting() {
        val path = filePath + SETTINGS_PATH
        deleteFileContent(path)
        try {
            FileWriter(path).use { file ->
                val json = JSONObject(objectMapper.convertValue(data, Map::class.java)).toString()
                file.write("var rplayer = $json;")
                Log.i(TAG, "Successfully Copied JSON Object to File...")
                HyperLog.i(TAG.toLowerCase(), "Successfully Copied JSON Object to File...")
                pushLogsOnElasticSearch(i, "Successfully Copied JSON Object to File...")
                Log.i(TAG, json)
                listener.onWriteInSetting(true)
            }
        } catch (e: Exception) {
            Log.e(TAG, "Exception " + e.printStackTrace())
            HyperLog.e(TAG.toLowerCase(), "${e.message} : ${TAG.toUpperCase()}")
            pushLogsOnElasticSearch(Constant.e, "${e.message} : ${TAG.toUpperCase()}")
        }
    }

    fun readSettingFile(filePath: String): JSONObject {
        return JSONObject(fileContent(filePath))
    }

    /**
     * Function used to check and return a file content
     */
    private fun fileContent(filename: String): String {
        var result = ""
        try {
            val br = BufferedReader(FileReader(filename))
            val sb = StringBuilder()
            var line = br.readLine()
            while (line != null) {
                sb.append(line)
                line = br.readLine()
            }
            result = sb.toString()
        } catch (e: Exception) {
            Log.e(TAG, "Exception " + e.printStackTrace())
            HyperLog.e(TAG.toLowerCase(), "${e.message} : ${TAG.toUpperCase()}")
            pushLogsOnElasticSearch(Constant.e,"${e.message} : ${TAG.toUpperCase()}")
        }
        return result
    }

    /**
     * Function used to remove a file content
     */
    fun deleteFileContent(filePath: String) {
        try {
            val file = FileWriter(filePath)
            file.write("")
        } catch (e: Exception) {
            HyperLog.e(TAG.toLowerCase(), "${e.message} : ${TAG.toUpperCase()}")
            pushLogsOnElasticSearch(Constant.e, "${e.message} : ${TAG.toUpperCase()}")
        }
    }

    /**
     * FileManagerHelper callback used by activities and/or fragments that
     * implement this object to allow interaction in this object
     */
    interface FileManagerCallback {
        fun onWriteInSetting(hasWriteInSetting: Boolean)
    }
}
