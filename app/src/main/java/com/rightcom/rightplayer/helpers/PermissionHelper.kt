package com.rightcom.rightplayer.helpers

import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import com.rightcom.rightplayer.fragments.CampaignFragment

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PermissionHelper.PermissionHelperCallback] interface
 * to handle interaction events.
 * Use the [PermissionHelper.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class PermissionHelper : Fragment() {

    /**
     * This helper is stateless fragment. The class help your to
     * check permissions state and to request them if permissions not granted.
     * To use this helper call the function {@link checkPermission()} by passing
     * arguments and implement callbacks. It's important to call the function
     * {@link destroyFragment()} when callbacks are send.
     */


    private lateinit var mContext: Context
    private lateinit var mListener: PermissionHelperCallback
    private lateinit var mPermissions: Array<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (permissionGranted(mPermissions)) { // Check if permission are granted
            mListener.permissionGranted() // Send call back to notify that permission are granted
        } else {
            requestPermissions(mPermissions, REQUEST_PERMISSION_CODE) // Request permission
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSION_CODE) {
            if (grantResults.isNotEmpty()) {
                if (permissionGranted(mPermissions)) {
                    mListener.permissionGranted()
                } else{
                    mListener.permissionRefuse()
                }
            } else {
                mListener.permissionRefuse()
            }
        }
    }

    /**
     * Function to check if permission are granted
     */
    private fun permissionGranted(permissions: Array<String>): Boolean {
        permissions.forEach { it ->
            if ((ContextCompat.checkSelfPermission(mContext, it) != PackageManager.PERMISSION_GRANTED)) {
                return false
            }
        }
        return true
    }

    companion object {
        const val TAG = "PermissionHelper"
        private const val REQUEST_PERMISSION_CODE = 100

        private fun newInstance(context: Context, listener: PermissionHelperCallback, permissions: Array<String>): PermissionHelper {
            val fragment = PermissionHelper()
            fragment.mContext = context
            fragment.mListener = listener
            fragment.mPermissions = permissions
            return fragment
        }

        fun checkPermission(context: Context, fragmentManager: FragmentManager, listener: PermissionHelperCallback, permissions: Array<String>) {
            val fragment = fragmentManager.findFragmentByTag(TAG)
            if (fragment != null) {
                fragmentManager.beginTransaction().remove(fragment).commit()
            }
            fragmentManager.beginTransaction().add(newInstance(context, listener, permissions), TAG).commit()
        }

        fun destroyFragment(fragmentManager: FragmentManager) {
            val fragment = fragmentManager.findFragmentByTag(TAG)
            if (fragment != null) {
                fragmentManager.beginTransaction().remove(fragment).commit()
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    interface PermissionHelperCallback {
        fun permissionGranted()
        fun permissionRefuse()
    }
}