package com.rightcom.rightplayer.helpers

import android.util.Log
import com.google.gson.GsonBuilder
import com.hypertrack.hyperlog.HyperLog
import com.rightcom.rightplayer.player
import com.rightcom.rightplayer.prefs
import com.rightcom.rightplayer.tools.Constant
import com.rightcom.rightplayer.tools.Constant.EMIT_MY_PING
import com.rightcom.rightplayer.tools.Constant.e
import com.rightcom.rightplayer.tools.Constant.i
import com.rightcom.rightplayer.tools.Constant.w
import com.rightcom.rightplayer.tools.Utils.pushLogsOnElasticSearch
import com.rightcom.rightplayer.tools.Utils.writeLog
import io.socket.client.Ack
import io.socket.client.IO
import io.socket.client.Socket
import org.apache.commons.lang3.StringUtils
import org.json.JSONObject

object SocketHelper {

    /**
     * This class help interaction with the socket.
     * With this class, we can request connection on
     * socket, request listening on channels, remove some listener...
     */

    val TAG = SocketHelper::class.java.simpleName.toUpperCase()
    var socket: Socket = IO.socket(Constant.SOCKET_ENDPOINT)

    private var listener: SocketListener? = null

    /**
     * This function try connection on socket and listen to the mistakes that could happen.
     * When connection success, he try to join room if player data already exist
     */
    fun connect() {
        socket.on(Socket.EVENT_CONNECT) { _ ->
            Log.i(TAG, "Connected to socket server")
            writeLog("Connected to socket server", "info")
            HyperLog.i(TAG.toLowerCase(),"Connected to socket server. : ${TAG.toUpperCase()}")
            pushLogsOnElasticSearch(i, "Connected to socket server. : ${TAG.toUpperCase()}")

            // Check if data exist and try to join
            if (!player.hasJoinRoom) {
                if ((prefs.playerId != null && prefs.playerId?.isNotEmpty()!!)) {
                    val data = JSONObject()
                    data.put("room", "room_${prefs.playerId}")
                    data.put("player_code",  prefs.playerCode)

                    Log.i("DATA TO JOIN HELPLER", data.toString())

                    com.rightcom.rightplayer.socket.emit(Constant.EMIT_JOIN, data, Ack { _ ->
                        Log.i(TAG, "Player emit join success")
                        HyperLog.i(TAG.toLowerCase(), "Player successfully join rom. : ${TAG.toUpperCase()}")
                        pushLogsOnElasticSearch(i, "Player successfully join rom. : ${TAG.toUpperCase()}")

                        val obj = prefs.socketOn
                        if (obj?.player_code != null && obj.playerid != null && obj.publickey != null && obj.status != null) {
                            data.put("player_code", obj.player_code)
                            data.put("playerid", obj.playerid)
                            data.put("publickey", obj.publickey)
                            data.put("status", StringUtils.capitalize(prefs.playerStatus?.toLowerCase()))
                            data.put("event", obj.publickey)
                            data.put("mode",  if (obj.mode != null) "Offline" else "Online")

                            com.rightcom.rightplayer.socket.emit(EMIT_MY_PING, data, Ack {res ->
                                val jsonObj = JSONObject()
                                jsonObj.put("socket_response", GsonBuilder().create().toJson(res))
                                jsonObj.put("channel", EMIT_MY_PING)
                                jsonObj.put("ping_data", "$data")
                                HyperLog.i(TAG,"Player ping response stack : $jsonObj : ${TAG.toUpperCase()}")
                                pushLogsOnElasticSearch(i, "Player ping response stack : $jsonObj : ${TAG.toUpperCase()}")
                                // Log
                                Log.i(TAG, "Player emit my ping success")
                                writeLog("Player ping response", "info")
                            })
                            player.hasJoinRoom = true
                        } else {
                            Log.i("OBJ_ELSE", GsonBuilder().create().toJson(obj))
                            HyperLog.i(TAG, "${GsonBuilder().create().toJson(obj)} : ${TAG.toUpperCase()}")
                            pushLogsOnElasticSearch(i, "${GsonBuilder().create().toJson(obj)} : ${TAG.toUpperCase()}")
                        }
                    })
                }
            }
        }
        socket.on(Socket.EVENT_CONNECT_ERROR) { _ ->
            Log.e(TAG, "Connection to socket server error")
            writeLog("Connection to socket server error", "error")
            HyperLog.e(TAG.toLowerCase(), "Error while trying to connect to the socket server. : ${TAG.toUpperCase()}")
            pushLogsOnElasticSearch(e, "Error while trying to connect to the socket server. : ${TAG.toUpperCase()}")
            player.hasJoinRoom = false
        }
        socket.on(Socket.EVENT_CONNECT_TIMEOUT) { _ ->
            Log.i(TAG, "Time out when try connection to socket server")
            writeLog("Time out when try connection to socket server", "warning")
            HyperLog.i(TAG.toLowerCase(), "Time out while trying to connect to the socket server. : ${TAG.toUpperCase()}")
            pushLogsOnElasticSearch(i, "Time out while trying to connect to the socket server. : ${TAG.toUpperCase()}")
            player.hasJoinRoom = false
        }
        socket.on(Socket.EVENT_DISCONNECT) { _ ->
            Log.i(TAG, "Disconnect from socket server")
            writeLog("Disconnect from socket server", "info")
            HyperLog.i(TAG.toLowerCase(), "Disconnect from socket server. : ${TAG.toUpperCase()}")
            pushLogsOnElasticSearch(i, "Disconnect from socket server. : ${TAG.toUpperCase()}")
            player.hasJoinRoom = false
        }
        socket.connect()
    }

    /**
     *  This function put listen on different channel which
     *  @param listener
     *  @param channels
     */
    fun listen(listener: SocketListener, channels: ArrayList<String>) {
        this.listener = listener
        channels.forEach { i ->
            socket.on(i) { args ->
                Log.e("ON", i)
                this.listener?.onReceiveEvent(i, args)
            }
        }
    }

    /**
     * Function to remove listener on some channels
     */
    fun removeListener(channels: ArrayList<String>) {
        channels.forEach { i ->
            socket.off(i) { _ ->
                HyperLog.i(TAG, "Remove listener from $i : $TAG")
                pushLogsOnElasticSearch(i, "Remove listener from $i : $TAG")
            }
        }
    }


    /**
     * Interface class to notify implementer when receive event on channel
     */
    interface SocketListener {
        fun onReceiveEvent(channel: String, data: Any)
    }
}