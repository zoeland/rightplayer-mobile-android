package com.rightcom.rightplayer.helpers

import android.os.AsyncTask
import android.util.Log
import com.hypertrack.hyperlog.HyperLog
import com.rightcom.rightplayer.tools.Constant
import com.rightcom.rightplayer.tools.Constant.i
import com.rightcom.rightplayer.tools.Utils
import com.rightcom.rightplayer.tools.Utils._dirChecker
import com.rightcom.rightplayer.tools.Utils.pushLogsOnElasticSearch
import org.apache.commons.io.FileUtils.deleteDirectory
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream

object UnzipHelper {

    private lateinit var _zipFile: File
    private val TAG = "UnzipHelper".toUpperCase()
    private val storage: File = Utils.campaignsStorage
    private lateinit var listener: UnzipCallback

    /**
     *  This function prepare data to unzip file
     *  @param _zipFile
     *  @param listener
     */
    fun requestUnzip(_zipFile: File, listener: UnzipCallback) {
        this._zipFile = _zipFile
        this.listener = listener
        _dirChecker(storage.path)
        UnzippingTask().execute()
    }

    /**
     * This function manage file unzipping and emit on
     * listener the result of unzipping
     */
/*    private fun unzip() {
        try {
            Log.i(TAG, "Starting to unzip")
            val zipFile = ZipFile(_zipFile.absolutePath)
            zipFile.extractAll(storage.path)
            listener.onUnzip(true, folderPath())
        } catch (e: Exception) {
            listener.onUnzip(false, "")
        }
    }*/

    private class UnzippingTask: AsyncTask<Void?, Int?, Void?>() {

        internal var fileUnzipped: Boolean = false
//        private val zipFileSize = ZipFile(_zipFile.absolutePath).size()
        private val zipFileLength = _zipFile.length()

        override fun onPreExecute() {
            HyperLog.i(TAG, "PreExecute unzip process. : ${TAG.toUpperCase()}")
//            pushLogsOnElasticSearch(i, "PreExecute unzip process. : ${TAG.toUpperCase()}")
            Log.i(TAG, "Start unzip")
            listener.onUnzipStart()
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            HyperLog.i(TAG, "Unzipping process in background. : ${TAG.toUpperCase()}")
//            pushLogsOnElasticSearch(i, "Unzipping process in background. : ${TAG.toUpperCase()}")
            try {
                val fin = FileInputStream(_zipFile.absolutePath)
                val zin = ZipInputStream(fin)
                var ze: ZipEntry?
                var progress: Long = 0
                do {
                    ze = zin.nextEntry
                    if (ze == null)
                        break

                    Log.v(TAG, "Unzipping " + ze.name)

                    if (ze.isDirectory) {
                        _dirChecker(storage.path + "/" + ze.name)
                    } else {
                        val outputStream = FileOutputStream(storage.path + "/" + ze.name)
                        val buffer = ByteArray(2048)
                        var len: Int
                        do {
                            len = zin.read(buffer)
                            if (len == -1)
                                break

                            progress += len
                            if (zipFileLength > 0)
                                publishProgress((progress * 100 / zipFileLength).toInt())

                            outputStream.write(buffer, 0, len)
                        }while (true)
                        zin.closeEntry()
                        outputStream.close()
                    }
                } while (true)
                // Close all opened stream after task finish
                zin.close()
                fileUnzipped = true
            } catch (e: Exception) {
                HyperLog.e(TAG, "${e.message} : ${TAG.toUpperCase()}")
                pushLogsOnElasticSearch(Constant.e, "${e.message} : ${TAG.toUpperCase()}")
                fileUnzipped = false
                deleteDirectory(File(folderPath()))
            }
            return null
        }

        override fun onProgressUpdate(vararg progress: Int?) {
            super.onProgressUpdate(*progress)
            listener.onUnzipProgressUpdated(progress[0]!!)
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            HyperLog.i(TAG, "Finish unzip process in background. : ${TAG.toUpperCase()}")
            pushLogsOnElasticSearch(i, "Finish unzip process in background. : ${TAG.toUpperCase()}")
            if (fileUnzipped) {
                listener.onUnzip(fileUnzipped, folderPath())
            } else {
                listener.onUnzip(fileUnzipped, "")
            }
        }
    }


    /**
     * When finish file unzipping, call this function to get file unzip folder
     */
    private fun folderPath(): String {
        return _zipFile.path.replace("Templates", "Campaigns").replace(".zip", "")
    }

    /**
     * This is the interface that the unzip requester must be implement
     * to receive the callback when unzipping finish
     */
    interface UnzipCallback{
        /**
         * The callback
         */
        fun onUnzipStart()

        /**
         * The callback
         * @param progress The unzip process rate
         */
        fun onUnzipProgressUpdated(progress: Int)

        /**
         * The callback
         * @param hasBeenUnzip : explain if the unzipping has been success
         * @param folderPath : path to the unzip folder
         */
        fun onUnzip(hasBeenUnzip: Boolean, folderPath: String)
    }
}