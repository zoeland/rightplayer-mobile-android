package com.rightcom.rightplayer.helpers

import android.os.AsyncTask
import android.util.Log
import com.hypertrack.hyperlog.HyperLog
import com.rightcom.rightplayer.tools.Constant
import com.rightcom.rightplayer.tools.Constant.e
import com.rightcom.rightplayer.tools.Constant.i
import com.rightcom.rightplayer.tools.Utils._dirChecker
import com.rightcom.rightplayer.tools.Utils.logsFileStorage
import com.rightcom.rightplayer.tools.Utils.pushLogsOnElasticSearch
import java.io.*
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

object ZipHelper {
    private lateinit var filePath: String
    private lateinit var zipFile: String
    val TAG = ZipHelper::class.java.simpleName.toUpperCase()
    private lateinit var listener: ZipCallback

    /**
     *  This function prepare data to zip text file
     *  @param textFile
     *  @param listener
     */
    fun requestZip(filePath: String, zipFile: String, listener: ZipCallback) {
        this.filePath = filePath
        this.zipFile = zipFile
        this.listener = listener

        _dirChecker(logsFileStorage.path)
        ZipHelper.ZippingTask().execute()
    }

    private class ZippingTask: AsyncTask<Void?, Int?, Void?>() {

        internal var fileZipped: Boolean = false

        override fun onPreExecute() {
            HyperLog.i(TAG, "PreExecute zipping process. : ${TAG.toUpperCase()}")
            pushLogsOnElasticSearch(i, "PreExecute zipping process. : ${TAG.toUpperCase()}")
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            try {
                val origin: BufferedInputStream?
                val dest = FileOutputStream(zipFile)
                val out = ZipOutputStream(BufferedOutputStream(
                        dest))
                val data = ByteArray(2048)

                Log.v("Compress", "Adding: $filePath")
                HyperLog.i(TAG, "Zipping $filePath : ${TAG.toUpperCase()}")
                pushLogsOnElasticSearch(i, "Zipping $filePath : ${TAG.toUpperCase()}")
                val fi = FileInputStream(filePath)
                origin = BufferedInputStream(fi, 2048)

                val entry = ZipEntry(filePath.substring(filePath.lastIndexOf("/") + 1))
                out.putNextEntry(entry)
                var count: Int

                do {
                    count = origin.read(data, 0, 2048)
                    if (count == -1)
                        break
                    out.write(data, 0, count)
                } while (true)

                origin.close()
                out.close()
                fileZipped = true
            } catch (e: Exception) {
                HyperLog.e(TAG, "${e.message} : ${TAG.toUpperCase()}")
                pushLogsOnElasticSearch(Constant.e, "${e.message} : ${TAG.toUpperCase()}")
            }

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            HyperLog.i(TAG, "Finish zipping process in background. : ${TAG.toUpperCase()}")
            pushLogsOnElasticSearch(i, "Finish zipping process in background. : ${TAG.toUpperCase()}")
            listener.onZip(fileZipped)
        }
    }

    /**
     * This is the interface that the unzip requester must be implement
     * to receive the callback when unzipping finish
     */
    interface ZipCallback{
        /**
         * The callback
         * @param hasBeenUnzip : explain if the unzipping has been success
         * @param folderPath : path to the unzip folder
         */
        fun onZip(hasBeenZipped: Boolean)
    }
}