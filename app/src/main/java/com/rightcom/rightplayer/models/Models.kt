package com.rightcom.rightplayer.models

import android.os.Parcel
import android.os.Parcelable

class Models {

    /**
     * This class are created to define inside all data model
     */

    data class ApiResponse<T>(var status: Int, var message: String, var data: T)

    data class IpInfo(
            var `as`: String?, var city: String?, var country: String?, var countryCode: String?,
            var isp: String?, var lat: Double?, var lon: Double?, var org: String?, var query: String?,
            var region: String?, var regionName: String?, var status: String?, var timezone: String?, var zip: String?)

    data class GenerateCode (var player_code: String?, var image: String?, var status: String?, var playerid: String?)

    data class NameValuePairs(var playerid: String?, var publickey: String?, var version: Int?, var action: String?,
                              var player_code: String?, var event: String?, var reload_at: String?, var relaunch_at: String?,
                              var status: String?, var mode: String?)

    /*data class Campaign(var id: String?, var rplayer: Any?, var template: String?,
                        var fromTemplate: Boolean, var template_folder: String?, var campaign_name: String?)*/

    class Campaign : Parcelable {

        var id: String = ""
        var rplayer: Any? = null
        var template: String = ""
        var fromTemplate: Boolean = false
        var template_folder: String = ""
        var campaign_name: String = ""
        var editor_code: EditorCode? = null
        var from_v2: Boolean = false


        constructor()

        constructor(parcel: Parcel) : this() {
            id = parcel.readString()
            template = parcel.readString()
            fromTemplate = parcel.readByte() != 0.toByte()
            template_folder = parcel.readString()
            campaign_name = parcel.readString()
            from_v2 = parcel.readByte() != 0.toByte()
        }

        constructor(id: String, rplayer: Any?, template: String, fromTemplate: Boolean, template_folder: String, campaign_name: String, editor_code: EditorCode?, from_v2: Boolean) {
            this.id = id
            this.rplayer = rplayer
            this.template = template
            this.fromTemplate = fromTemplate
            this.template_folder = template_folder
            this.campaign_name = campaign_name
            this.editor_code = editor_code
            this.from_v2 = from_v2
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(id)
            parcel.writeString(template)
            parcel.writeByte(if (fromTemplate) 1 else 0)
            parcel.writeString(template_folder)
            parcel.writeString(campaign_name)
            parcel.writeByte(if (from_v2) 1 else 0)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<Campaign> {
            override fun createFromParcel(parcel: Parcel): Campaign {
                return Campaign(parcel)
            }

            override fun newArray(size: Int): Array<Campaign?> {
                return arrayOfNulls(size)
            }
        }
    }

    data class EditorCode(var html: String)
    data class CampaignExist(var inCampaign: Boolean, var inTemplate: Boolean, var path: String)
}