package com.rightcom.rightplayer.models

class Player private constructor() {

    /**
     * This class is the singleton which store player
     * data like state... along player life. The data are
     * deleted when player is closed
     */

    var hasJoinRoom = false
    var publickey: String? = null

    companion object {
        val instance = Player()
    }

    /**
     * This is enumeration of different type of the player state
     */
    enum class State {
        ACTIVE,
        UNACTIVE,
        DELETED,
        PLAYING,
        PENDING
    }
}
