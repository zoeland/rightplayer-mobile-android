package com.rightcom.rightplayer.models

import android.content.Context
import com.google.gson.GsonBuilder

class Prefs(context: Context) {

    /**
     * This class manage storage and recovery of player data in the shared preferences
     */

    var sp = context.getSharedPreferences("RightPlayer", Context.MODE_PRIVATE)

    var playerId: String?
        get() = sp.getString("PlayerId", "")
        set(value) = sp.edit().putString("PlayerId", value).apply()

    var playerCode: String?
        get() = sp.getString("PlayerCode", "")
        set(value) = sp.edit().putString("PlayerCode", value).apply()

    var playerMode: String?
        get() = sp.getString("PlayerMode", "")
        set(value) = sp.edit().putString("PlayerMode", value).apply()

    var playerStatus: String?
        get() = sp.getString("PlayerStatus", "")
        set(value) = sp.edit().putString("PlayerStatus", value).apply()

    var voiceticketactivated: Int
        get() = sp.getInt("VoiceTicketActivated", -1)
        set(value) = sp.edit().putInt("VoiceTicketActivated", value).apply()

    var campaignPath: String?
        get() = sp.getString("IndexPath", "")
        set(value) = sp.edit().putString("IndexPath", value).apply()

    var socketOn: Models.NameValuePairs?
        get() = GsonBuilder().create().fromJson(sp.getString("NameValuePairs", null), Models.NameValuePairs::class.java)
        set(value) = sp.edit().putString("NameValuePairs", GsonBuilder().create().toJson(value)).apply()

    var campaign: Models.Campaign?
        get() = GsonBuilder().create().fromJson(sp.getString("Campaign", null), Models.Campaign::class.java)
        set(value) = sp.edit().putString("Campaign", GsonBuilder().create().toJson(value)).apply()

    /**
     * This function check if player data exist in shared preferences and are not null or empty.
     * If player data exist and status of player is "PLAYING", we check if data for playing
     * exist and not null or empty
     */
    fun prefDataAreNotEmpty(): Boolean {
        return if (playerId != null && playerId?.isNotEmpty()!!
                && playerCode != null && playerCode?.isNotEmpty()!!
                && playerMode != null && playerMode?.isNotEmpty()!!
                && playerStatus != null && playerStatus?.isNotEmpty()!!) {
            if (playerStatus?.toUpperCase() != Player.State.PLAYING.name) {
                true
            } else {
                campaign != null
            }
        } else {
            false
        }
    }
}