package com.rightcom.rightplayer.services

import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import com.hypertrack.hyperlog.HyperLog
import com.rightcom.rightplayer.tools.Constant.i
import com.rightcom.rightplayer.tools.Utils.autoLaunchApp
import com.rightcom.rightplayer.tools.Utils.pushLogsOnElasticSearch


class AutoStartApp : BroadcastReceiver() {
    private val HLTAG = AutoStartApp::class.java.simpleName
    /**
     * Function call when app receive a boot signal from user's device
     */
    override fun onReceive(context: Context, intent: Intent) {
        HyperLog.i(HLTAG, "Received reboot signal from device. : ${HLTAG.toUpperCase()}")
        pushLogsOnElasticSearch(i, "Received reboot signal from device. : ${HLTAG.toUpperCase()}")
        autoLaunchApp(context, context.applicationContext.packageName)
    }
}