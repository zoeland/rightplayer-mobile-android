package com.rightcom.rightplayer.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.hypertrack.hyperlog.HyperLog
import com.rightcom.rightplayer.appContext
import com.rightcom.rightplayer.helpers.ZipHelper
import com.rightcom.rightplayer.tools.Constant.HL_LOG_FILE_NAME
import com.rightcom.rightplayer.tools.Constant.e
import com.rightcom.rightplayer.tools.Constant.i
import com.rightcom.rightplayer.tools.Utils.modifyLogsDirectoryContent
import com.rightcom.rightplayer.tools.Utils.logsFileStorage
import com.rightcom.rightplayer.tools.Utils.pushLogsOnElasticSearch
import java.io.File

class LogFileArchiveScheduler : BroadcastReceiver(), ZipHelper.ZipCallback {

    private val TAG = LogFileArchiveScheduler::class.java.simpleName
    private var hlFile: File? = null

    /**
     * Function call when app receive a boot signal from user's device
     */
    override fun onReceive(context: Context?, intent: Intent?) {
        hlFile = HyperLog.getDeviceLogsInFile(appContext, "$HL_LOG_FILE_NAME.txt", false)
        val txt = hlFile?.absolutePath!!.substring(hlFile?.absolutePath!!.lastIndexOf(".") + 1)
        val zipFileName = hlFile?.absolutePath!!.replace(txt, "zip").substring(hlFile?.absolutePath!!.lastIndexOf("/") + 1)
        ZipHelper.requestZip(hlFile?.absolutePath!!, "$logsFileStorage/$zipFileName", this)

        HyperLog.i(TAG, "RightPlayer's logs file zipping process alarm fired. : ${TAG.toUpperCase()}")
        pushLogsOnElasticSearch(i, "RightPlayer's logs file zipping process alarm fired. : ${TAG.toUpperCase()}")
    }

    /**
     * Callback receive from ZipHelper when app logs file compression process finish
     */
    override fun onZip(hasBeenZipped: Boolean) {
        if (hasBeenZipped) {
            HyperLog.i(TAG, "RightPlayer's logs file has successfully been zipped. : ${TAG.toUpperCase()}")
            pushLogsOnElasticSearch(i,"RightPlayer's logs file has successfully been zipped. : ${TAG.toUpperCase()}")
            hlFile?.delete()
            modifyLogsDirectoryContent()
        } else {
            HyperLog.e(TAG, "RightPlayer's logs file zipping failed. : ${TAG.toUpperCase()}")
            pushLogsOnElasticSearch(e, "RightPlayer's logs file zipping failed. : ${TAG.toUpperCase()}")
        }
    }
}