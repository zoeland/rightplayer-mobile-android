package com.rightcom.rightplayer.tools

import android.annotation.SuppressLint
import android.os.Build
import android.provider.Settings
import com.jaredrummler.android.device.DeviceName
import com.rightcom.rightplayer.BuildConfig
import com.rightcom.rightplayer.R
import com.rightcom.rightplayer.appContext
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object Constant {

    // Date format
    const val DATE_TIME_FORMAT = "dd-MM-yy HH:mm:ss"

    @SuppressLint("SimpleDateFormat")
    val dateFormat: DateFormat = SimpleDateFormat(DATE_TIME_FORMAT)

    // File
    const val APP_LOG_FILE_NAME  = "RightPlayer.log"
    val HL_LOG_FILE_NAME  = appContext.getString(R.string.app_name).toLowerCase()+ "_" + Utils.getYesterDayDate()

    var RUNNING = false

    // API CONSTANT
    const val ACTION_CHECK = "check"
    const val ACTION_GENERATE = "generate"
    const val PLAYER = "Android"
    const val SIGNATURE = "AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHx0IYM"
    const val PLAYER_TYPE = "mobile-android"
    const val VERSION_CODE = ""+BuildConfig.VERSION_CODE
    const val VERSION_NAME = BuildConfig.VERSION_NAME
    var DEVICE_NAME = ""+DeviceName.getDeviceName()
    var DEVICE_OS = "Android ${Build.VERSION.RELEASE}"
    var RELEASE_DATE = "03-10-18 02:56:15"
    @SuppressLint("HardwareIds")
    val DEVICE_UUID = Settings.Secure.getString(appContext.contentResolver, Settings.Secure.ANDROID_ID)

    // LINKS
    const val URL_BASE="http://playemi-v1.right-com.com/v2/"
    const val SOCKET_ENDPOINT = "http://playemi-v1.right-com.com/player"
    const val IP_LOCATION_URL="http://www.ip-api.com/"
    const val WATCH_DEMO_URL="https://vimeo.com/181002384"

    // SOCKET CHANNEL --> emit
    const val EMIT_JOIN = "join" // join un room
    const val EMIT_MY_PING = "myping" // répondre à un ping et envoyer des données
    const val EMIT_READY = "ready" // signaler que le player est ready
    const val EMIT_DEACTIVATE = "deactivate" // signaler que la player a été désactivé
    const val EMIT_OS_STATUS = "os_status" // envoyer des données du l'OS
    const val EMIT_PLAYING = "playing" // signaler que le player joue
    const val EMIT_IP_ADDRESS = "ip_adress_response" // envoyer l'IP du player

    // SOCKET CHANNEL --> on
    const val ON_GET_IP_SERVER = "get_ip_server" // get ip
    const val ON_RELOAD = "reload" // reload app
    const val ON_RELAUNCH = "relaunch" // relaunch app
    const val ON_SEND_COMMAND = "send_command" // send command
    const val ON_UPDATE_PLAYER = "update_player" // pour faire une mise à jour automatique
    const val ON_MOVETO = "moveto" // changer le statut du player
    const val ON_CHANGE_MODE = "change_mode" // changer le mode du player
    const val ON_REFRESH = "refresh" // pour refresh l'app
    const val ON_SCHEDULE_INFO = "schedule_info" // pour recevoir les info de programmation
    const val ON_SCHEDULE_DELETE_INFO = "schedule_delete_info" // pour supprimer un programme
    const val ON_SCHEDULE_UPDATE_INFO = "schedule_update_info" // pour mettre à jour un programme
    const val ON_PLAY_NOW = "play_now" // quand on publie une campagne

    // COMMAND ACTION
    const val ACTION_PING = "ping" // Ping the device
    const val ACTION_GET_INFO = "get_info" // Get player info
    const val ACTION_SHUTDOWN = "shutdown" // Shutdown ap

    // PLAYER MODE
    const val MODE_OFFLINE = "Offline"
    const val MODE_ONLINE = "Online"

    // Campaign files path
    const val SETTINGS_PATH = "/js/settings.js"

    // ElasticSearch url
    const val LOGSTASH_BASE_URL = "http://109.74.86.120:18402/" // ELASTIC_SEARCH: {HOST: ‘109.74.86.120’, PORT: 18401, PROTOCOL: ‘http’, USERNAME: ‘’, PASSWORD: ‘’, TTL: ‘’, }
    const val LOGSTASH_IP_ADDRESS = "109.74.86.120"
    const val LOGSTASH_IP_PORT = 18402
    const val KIBANA_IP_PORT = 18404

    // Log level name
    const val i = "info"
    const val e = "error"
    const val v = "verbose"
    const val d = "debug"
    const val w = "warning"
    const val a = "assert"

    /* VIDEO_URL_KEY */
    const val VIDEO_URL_1 = "video_url_1"
    const val VIDEO_URL_2 = "video_url_2"
    const val VIDEO_URL_3 = "video_url_3"
    const val VIDEO_URL_4 = "video_url_4"
    const val VIDEO_URL_5 = "video_url_5"
    const val VIDEO_URL_6 = "video_url_6"
    const val VIDEO_URL_7 = "video_url_7"
    const val VIDEO_URL_8 = "video_url_8"
    const val VIDEO_URL_9 = "video_url_9"
    const val VIDEO_URL_10 = "video_url_10"
    const val VIDEO_URL_11 = "video_url_11"
    const val VIDEO_URL_12 = "video_url_12"
    const val VIDEO_URL_13 = "video_url_13"
    const val VIDEO_URL_14 = "video_url_14"
    const val LOGO_VIDEO = "logo_video"

    /* RPLAYER_FILE_KEY */
    const val FILE_URL = "fileurl"
    const val FILE_NAME = "filename"
}