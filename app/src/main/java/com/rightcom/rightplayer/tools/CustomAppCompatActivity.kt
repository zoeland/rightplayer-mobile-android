package com.rightcom.rightplayer.tools

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager

/**
 * Created by Abdel-Faïcal on 16/07/2018.
 */
open class CustomAppCompatActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

        Utils.hideSystemUI(window.decorView)
        window.decorView.setOnSystemUiVisibilityChangeListener {
            Utils.hideSystemUI(window.decorView)
        }
    }
}