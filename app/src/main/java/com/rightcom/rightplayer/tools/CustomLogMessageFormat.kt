package com.rightcom.rightplayer.tools

import android.content.Context
import com.hypertrack.hyperlog.LogFormat
import com.rightcom.rightplayer.prefs
import com.rightcom.rightplayer.tools.Constant.DEVICE_OS
import com.rightcom.rightplayer.tools.Constant.DEVICE_UUID
import com.rightcom.rightplayer.tools.Constant.VERSION_NAME

internal class CustomLogMessageFormat(context: Context) : LogFormat(context) {

    override fun getFormattedLogMessage(logLevelName: String?,
                                        tag: String?,
                                        message: String?,
                                        dateTime: String?,
                                        senderName: String?,
                                        osVersion: String?,
                                        deviceUUID: String?): String {

        val mFormat = "$dateTime [RIGHTPLAYER_MOBILE] $VERSION_NAME $DEVICE_OS ${if (prefs.playerId.isNullOrEmpty()) "NO_PLAYER_ID" else prefs.playerId} ${logLevelName?.toLowerCase()} : $message"
        return "$dateTime [rightplayer_mobile] ${logLevelName?.toLowerCase()} : $message"
    }
}