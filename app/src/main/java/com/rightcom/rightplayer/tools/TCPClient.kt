package com.rightcom.rightplayer.tools

import android.util.Log
import com.hypertrack.hyperlog.HyperLog
import com.rightcom.rightplayer.tools.Constant.LOGSTASH_IP_ADDRESS
import com.rightcom.rightplayer.tools.Constant.LOGSTASH_IP_PORT
import java.io.*
import java.net.InetAddress
import java.net.Socket


class TCPClient(listener: OnMessageReceived) {

    val TAG = TCPClient::class.java.simpleName
    // message to send to the server
    private var mServerMessage: String? = null
    // sends message received notifications
    private var mMessageListener: OnMessageReceived? = listener
    // while this is true, the server will continue RUNNING
    private var mRun = false
    // used to send messages
    private var mBufferOut: PrintWriter? = null
    // used to read messages from the server
    private var mBufferIn: BufferedReader? = null

    /**
     * Sends the message entered by client to the server
     *
     * @param message text entered by client
     */
    fun sendMessage(message: String) {
        val runnable = Runnable {
            if (mBufferOut != null) {
                mBufferOut!!.println(message)
                mBufferOut!!.flush()
            }
        }
        val thread = Thread(runnable)
        thread.start()
        Log.i(TAG, "Send: $message")
    }

    /**
     * Close the connection and release the members
     */
    fun stopClient() {

        mRun = false

        if (mBufferOut != null) {
            mBufferOut!!.flush()
            mBufferOut!!.close()
        }

        mMessageListener = null
        mBufferIn = null
        mBufferOut = null
        mServerMessage = null
    }

    /**
     * Initialize connection and connect client to TCP server
     */
    fun run() {

        mRun = true

        try {
            //here you must put your computer's IP address.
            val serverAddress = InetAddress.getByName(LOGSTASH_IP_ADDRESS)

            Log.d("TCP Client", "C: Connecting...")

            //create a socket to make the connection with the server
            val socket = Socket(serverAddress, LOGSTASH_IP_PORT)
            Log.i(TAG, "Connect => ${socket.isConnected}")

            try {

                //sends the message to the server
                mBufferOut = PrintWriter(BufferedWriter(OutputStreamWriter(socket.getOutputStream())), true)

                //receives the message which the server sends back
                mBufferIn = BufferedReader(InputStreamReader(socket.getInputStream()))


                //in this while the client listens for the messages sent by the server
                while (mRun) {

                    mServerMessage = mBufferIn!!.readLine()

                    if (mServerMessage != null && mMessageListener != null) {
                        //call the method messageReceived from MyActivity class
                        mMessageListener!!.messageReceived(mServerMessage!!)
                    }

                }

                HyperLog.i(TAG, "RESPONSE FROM SERVER: Received Message: $mServerMessage : ${TAG.toUpperCase()}")

            } catch (e: Exception) {
                HyperLog.e(TAG, "Exception : ${e.message} : ${TAG.toUpperCase()}")
            } finally {
                //the socket must be closed. It is not possible to reconnect to this socket
                // after it is closed, which means a new socket instance has to be created.
                socket.close()
            }

        } catch (e: Exception) {
            HyperLog.e(TAG, "Exception : ${e.message} : ${TAG.toUpperCase()}")
        }

    }

    /**
     * Declare the interface. The method messageReceived(String message) shall be implemented
     * in the Activity class at on AsyncTask doInBackground
     */
    interface OnMessageReceived {
        fun messageReceived(message: String)
    }
}