package com.rightcom.rightplayer.tools

import android.content.Context
import android.graphics.Typeface

/**
 * Created by Abdel-Faïçal on 16/07/2018.
 */
object TypefaceManager {

    fun regularTypeFace(context: Context) : Typeface {
         return Typeface.createFromAsset(context.assets, "fonts/Questrial-Regular.ttf")
    }

    fun kaushanTypeFace(context: Context) : Typeface {
         return Typeface.createFromAsset(context.assets, "fonts/KaushanScript-Regular.ttf")
    }

    fun permanentMarkerTypeFace(context: Context) : Typeface {
         return Typeface.createFromAsset(context.assets, "fonts/PermanentMarker-Regular.ttf")
    }
}