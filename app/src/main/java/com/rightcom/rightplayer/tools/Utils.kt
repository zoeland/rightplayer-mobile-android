package com.rightcom.rightplayer.tools

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.AsyncTask
import android.os.Build
import android.os.Environment
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v7.content.res.AppCompatResources
import android.text.format.DateFormat
import android.util.Log
import android.view.View
import android.widget.TextView
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.zxing.BarcodeFormat
import com.hypertrack.hyperlog.HyperLog
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.rightcom.rightplayer.R
import com.rightcom.rightplayer.appContext
import com.rightcom.rightplayer.models.Models
import com.rightcom.rightplayer.player
import com.rightcom.rightplayer.prefs
import com.rightcom.rightplayer.tools.Constant.APP_LOG_FILE_NAME
import com.rightcom.rightplayer.tools.Constant.i
import org.apache.commons.io.FileUtils
import org.jetbrains.anko.doAsync
import org.json.JSONArray
import org.json.JSONObject
import java.io.*
import java.math.BigInteger
import java.net.NetworkInterface
import java.net.URI
import java.net.URISyntaxException
import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * Created by Abdel-Faïçal on 16/07/2018.
 */
object Utils {

    private val HLTAG = Utils::class.java.simpleName
    val gsonBuilder = GsonBuilder().create()
    val objectMapper = ObjectMapper()

    var appStorage = File(Environment.getExternalStorageDirectory().absolutePath + "/"+"RightPlayer")
    var templateStorage = File(Environment.getExternalStorageDirectory().absolutePath + "/"+"RightPlayer/Templates")
    var campaignsStorage = File(Environment.getExternalStorageDirectory().absolutePath + "/"+"RightPlayer/Campaigns")
    var downloadsStorage = File(Environment.getExternalStorageDirectory().absolutePath + "/"+"RightPlayer/Downloads")
    var customStorage = File(Environment.getExternalStorageDirectory().absolutePath + "/"+"RightPlayer/Custom")
    var logsFileStorage = File(Environment.getExternalStorageDirectory().absolutePath + "/"+"RightPlayer/Logs")

    private var mTCPClient: TCPClient? = null
    private var firstTime = false

    /**
     * Function to hide system UI
     */
    fun hideSystemUI(view: View) {
        view.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE)
    }

    /**
     * Function to load fragment with different params
     */
    fun loadFragment(fragmentManager: FragmentManager, fragment: Fragment,
                     addToBackStack: Boolean,
                     enterAnimation: Int,
                     exitAnimation: Int,
                     popEnterAnimation: Int,
                     popExitAnimation: Int) {
        // Remove fragment if it exit
        if (fragmentManager.findFragmentByTag(fragment.tag) != null) {
            fragmentManager.beginTransaction().remove(fragmentManager.findFragmentByTag(fragment.tag)).commit()
        }

        // Prepare fragment loading
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(enterAnimation, exitAnimation, popEnterAnimation, popExitAnimation)
        if (addToBackStack){
            fragmentTransaction.addToBackStack(null)
        }
        fragmentTransaction.add(R.id.frameLayout, fragment).commit()
    }

    /**
     * Function used to replace a fragment by another one inside a frame
     */
    fun loadFragment(fragmentManager: FragmentManager, fragment: Fragment, tag: String, addToBackStack: Boolean) {
        Log.i("LOADER", "Load fragment ${fragment.javaClass.name}")
        HyperLog.i(HLTAG, "Load fragment ${fragment.javaClass.name}. : ${HLTAG.toUpperCase()}")
        fragmentManager.beginTransaction().replace(R.id.frameLayout, fragment).addToBackStack(null).commit()
    }

    /**
     * Function to change type face of snack bar and show it
     */
    fun showSnackBar(context: Context, snackbar: Snackbar) {
        val sBarMainText = (snackbar.view).findViewById<TextView>(android.support.design.R.id.snackbar_text)
        val sBarActionText = (snackbar.view).findViewById<TextView>(android.support.design.R.id.snackbar_action)

        sBarMainText.typeface = TypefaceManager.regularTypeFace(context)
        sBarActionText.typeface = TypefaceManager.regularTypeFace(context)
        snackbar.show()
    }

    /**
     * Function to generate QR Code as image and return bitmap
     */
    fun generateCode(text: String): Bitmap? {
        return try {
            val barcodeEncoder = BarcodeEncoder()
            return barcodeEncoder.encodeBitmap(text, BarcodeFormat.QR_CODE, 500, 500)
        } catch (e: Exception) {
            HyperLog.e(HLTAG, "${e.message} : ${HLTAG.toUpperCase()}")
            null
        }
    }

    /**
     * Function to check if external storage is mounted
     */
    fun sdCardPresent(): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }

    /**
     * This function check if the directory pass in argument exit and create it if it doesn't exist
     */
    fun _dirChecker(dir: String) {
        val f = File(dir)
        if (dir.length >= 0 && !f.isDirectory) {
            f.mkdirs()
        }
    }

    /**
     * Function to get file name from url
     */
    fun fileName(url: String): String {
        return try {
            val uri = URI(url)
            val segments = uri.path.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            segments[segments.size - 1]
        } catch (e: URISyntaxException) {
            HyperLog.e(HLTAG, "${e.message} : ${HLTAG.toUpperCase()}")
            ""
        }
    }

    /**
     * Get mac address of the app
     */
    fun macAddress(): String {
        try {
            val all=Collections.list(NetworkInterface.getNetworkInterfaces())
            for (nif in all) {
                if (!nif.name.equals("wlan0", ignoreCase=true)) continue
                val macBytes=nif.hardwareAddress ?: return ""

                val res1=StringBuilder()
                for (b in macBytes) {
                    res1.append(String.format("%02X:", b))
                }

                if (res1.isNotEmpty()) {
                    res1.deleteCharAt(res1.length - 1)
                }
                return res1.toString()
            }
        } catch (ex: Exception) {
            HyperLog.e(HLTAG, "${ex.message} : ${HLTAG.toUpperCase()}")
            return ""
        }
        return ""
    }

    /**
     * This function check if template already exist in local storage and return the path
     * @param url : template url
     * @return Models.CampaignExist
     */
    fun campaignExist(url: String): Models.CampaignExist {
        val fileName = fileName(url)

        val fileInCampaign =  File("${campaignsStorage.absolutePath}/${fileName.replace(".zip", "")}")
        val fileInTemplate =  File("${templateStorage.absolutePath}/$fileName")

        // Verify if data exist and delete it if content is empty
        if (fileInCampaign.exists() && !fileHaveContent(fileInCampaign)) {
            deleteDirectory(fileInCampaign)
        }
        if (fileInCampaign.exists() && fileHaveContent(fileInCampaign) && (prefs.campaignPath == null || prefs.campaignPath?.isEmpty()!!)) {
            deleteDirectory(fileInCampaign)
        }
        if (fileInTemplate.exists() && !fileHaveContent(fileInTemplate)) {
            deleteDirectory(fileInTemplate)
        }

        return when {
            fileInCampaign.exists() && fileInTemplate.exists()  -> {
                Models.CampaignExist(true, true, fileInCampaign.absolutePath)
            }
            fileInCampaign.exists() && !fileInTemplate.exists() -> {
                deleteDirectory(fileInCampaign)
                Models.CampaignExist(false, false, "")
            }
            fileInTemplate.exists() && !fileInCampaign.exists() -> {
                deleteDirectory(fileInCampaign)
                Models.CampaignExist(false, true, fileInTemplate.absolutePath)
            }
            else -> {
                deleteDirectory(fileInCampaign)
                deleteDirectory(fileInTemplate)
                Models.CampaignExist(false, false, "")
            }
        }
    }

    /**
     * Check if file have content
     */
    private fun fileHaveContent(file: File): Boolean {
        return (file.length() / 1024).toInt() > 0
    }

    /**
     * Function used to delete a directory and it contents
     * @param file The file to delete
     */
    private fun deleteDirectory(file: File): Boolean {
        if(file.exists() ) {
            val files = file.listFiles() ?: return true
            for (i in files) {
                if(i.isDirectory) {
                    deleteDirectory(i)
                }
                else {
                    i.delete()
                }
            }
        }
        return(file.delete())
    }

    /**
     * Function used to generate a unique random string key
     * @param length The length of the generated unique random string key
     */
    fun saltString(length: Int): String {
        val saltchars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toLowerCase()
        val salt = StringBuilder()
        val rnd = Random()
        while (salt.length < length) { // length of the random string.
            val index = (rnd.nextFloat() * saltchars.length).toInt()
            salt.append(saltchars[index])
        }
        return salt.toString()
    }

    /**
     * We create unique id based on the mac address. It this id which will be
     * send to the server to request player code
     * @return String
     */
    fun uniqueID(): String {
        val key = saltString(100) + java.sql.Timestamp(System.currentTimeMillis()).time
        return BigInteger(1, MessageDigest.getInstance("MD5")
                .digest(key.toByteArray())).toString(16).substring(0,30)
    }

    /**
     * Function to get local ip address
     */
    fun ipAddress(): String {
        try {
            val interfaces = Collections.list(NetworkInterface.getNetworkInterfaces())
            for (intf in interfaces) {
                val addrs = Collections.list(intf.inetAddresses)
                for (addr in addrs) {
                    if (!addr.isLoopbackAddress) {
                        val sAddr = addr.hostAddress
                        sAddr.indexOf(':')<0
                        return sAddr
                    }
                }
            }
        } catch (e: Exception) { HyperLog.e(HLTAG, "${e.message} : ${HLTAG.toUpperCase()}")}
        return ""
    }

    /**
     * Function to check if device have access to internet by ping 8.8.8.8
     */
    fun deviceHasInternetAccess(): Boolean {
        val runtime = Runtime.getRuntime()
        try {
            val process = runtime.exec("system/bin/ping -c 1 8.8.8.8")
            val value = process.waitFor()
            if (value == 0)
                return true
        } catch (e: IOException) {
//            e.printStackTrace()
            HyperLog.e(HLTAG, "${e.message} : ${HLTAG.toUpperCase()}")
        } catch (e: InterruptedException) {
//            e.printStackTrace()
            HyperLog.e(HLTAG, "${e.message} : ${HLTAG.toUpperCase()}")
        }
        return false
    }

    /**
     * Function to insert hyphen in string
     */
    fun insertHyphenInString(s: String): String {
        val sb = StringBuilder(s)
        for (i in 0 until s.length - 1)
            sb.insert(i + (i + 1), " ")

        return sb.toString()
    }

    fun getBitmapFromVectorDrawable(context: Context, drawableId: Int): Bitmap {
        val drawable = try {
            ContextCompat.getDrawable(context, drawableId)
        } catch (e: Exception) {
            AppCompatResources.getDrawable(context, drawableId)
        }
        val bitmap = Bitmap.createBitmap(drawable!!.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

    /**
     * Function to convert a json array to object
     */
    fun fromJsonArray(jsonArray: String): Models.NameValuePairs {
        val array = JSONArray(jsonArray)
        val obj = array.getJSONObject(0)
        return GsonBuilder().create().fromJson<Models.NameValuePairs>(obj.getString("nameValuePairs"),
                Models.NameValuePairs::class.java)
    }

    /**
     * Function to convert a json array to campaign object
     */
    fun fromJsonToCampaign(json: String): Models.Campaign? {
        try {
            val array = JSONArray(json)
            val obj = array.getJSONObject(0)
                    .getJSONObject("nameValuePairs")
                    .getJSONObject("data")
                    .getJSONObject("nameValuePairs")
                    .getJSONObject("campaign")
                    .getJSONObject("nameValuePairs")

            val fromTemplate = obj.getBoolean("fromTemplate")
            val campaignName = obj.getString("campaign_name")
            val id = obj.getString("id")
            val from_v2 = obj.optBoolean("from_v2")

            var rplayer: Any? = null
            var conf: Any? = null
            var template = ""
            var templateFolder = ""
            var editor_code: Models.EditorCode? = null
            if (fromTemplate) {
                rplayer = obj.getJSONObject("rplayer")
                conf = GsonBuilder().create().fromJson<Any>(rplayer.getString("nameValuePairs"), Any::class.java)
                template = obj.getString("template")
                templateFolder = obj.getString("template_folder")
            } else {
                editor_code = Utils.gsonBuilder.fromJson(obj
                        .getJSONObject("editor_code")
                        .getJSONObject("nameValuePairs").toString(), Models.EditorCode::class.java)
            }

            val campaign = Models.Campaign(id, conf, template, fromTemplate, templateFolder, campaignName, editor_code, from_v2)
            Log.i("CAMPAIGN", GsonBuilder().create().toJson(campaign))
            return campaign
        } catch (e: Exception) {
            HyperLog.e(HLTAG, "${e.message} : ${HLTAG.toUpperCase()}")
            pushLogsOnElasticSearch(Constant.e, "${e.message} : ${HLTAG.toUpperCase()}")
        }
        return null
    }

    /**
     * Function to create(if not exist) and write into a log file
     */
    fun writeLog(s: String, type: String) {
        val TAG = "Write_log_exception".toUpperCase()
        var message = "${Constant.dateFormat.format(Date())} : $s"
        val oldcontent = readLog(appContext)
        message = "{$type}:$message\n$oldcontent"
        val outputStream: FileOutputStream
        try {
            outputStream = appContext.openFileOutput(APP_LOG_FILE_NAME, Context.MODE_PRIVATE)
            outputStream.write(message.toByteArray())
            outputStream.close()
        } catch (e: Exception) {
//            e.printStackTrace()
            Log.e(TAG.toUpperCase(), "${e.message}")
            HyperLog.e(HLTAG, "${e.message} : ${HLTAG.toUpperCase()}")
        }
    }

    /**
     * Function to read an existing log file
     */
    fun readLog(context: Context): String {
        val TAG = "Read_log_exception".toUpperCase()
        var content = ""
        try {
            val inputStream: InputStream
            inputStream = context.openFileInput(APP_LOG_FILE_NAME)
            if ( inputStream != null ) {
                val inputStreamReader = InputStreamReader(inputStream)
                val bufferedReader = BufferedReader(inputStreamReader)
                var receiveString: String?
                val stringBuilder = StringBuilder()
                do {
                    receiveString = bufferedReader.readLine()
                    if (receiveString == null)
                        break
                    stringBuilder.append(receiveString + "\n")
                } while (true)
                /*while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString + "\n")
                }*/
                inputStream.close()
                content = stringBuilder.toString()
            }
        }
        catch (e: FileNotFoundException) {
            Log.e(TAG.toUpperCase(), "File not found: ${e.message}")
            HyperLog.e(HLTAG, "File not found: ${e.message} : ${HLTAG.toUpperCase()}")
        } catch (e: IOException) {
            Log.e(TAG.toUpperCase(), "Can not read file: ${e.message}")
            HyperLog.e(HLTAG, "Can not read file: ${e.message} : ${HLTAG.toUpperCase()}")
//            e.printStackTrace()
        } catch (e: IOException) {
//            e.printStackTrace()
            HyperLog.e(HLTAG, "${e.message} : ${HLTAG.toUpperCase()}")
        }
        return content
    }

    /**
     * Clear application tasks and restart it
     */
    fun autoLaunchApp(context: Context, packageName: String) {
        try {
            val intent: Intent = context.applicationContext.packageManager.getLaunchIntentForPackage(packageName)
            Objects.requireNonNull(intent).addCategory(Intent.CATEGORY_LAUNCHER)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            context.startActivity(intent)
        } catch (e: Exception) {
            HyperLog.e(HLTAG, "${e.message} : ${HLTAG.toUpperCase()}")
        }
    }

    /**
     * Convert a json to HashMap
     */
    fun jsonToMap(json: Any): HashMap<String, String> {
        val map = HashMap<String, String>()
        try {
            val obj = JSONObject(Gson().toJson(json))
            val keysItr = obj.keys()
            while(keysItr.hasNext()) {
                val key = keysItr.next()
                val value = obj.get(key).toString()
                map[key] = value
            }
        } catch (e: Exception) {
            Log.e("Exception", e.message)
            HyperLog.e(HLTAG, "${e.message} : ${HLTAG.toUpperCase()}")
        }
        return map
    }

    /**
     * Check if campaign settings file has already been download
     */
    fun campaignDownloadedSettingsFileExist(path: String): Boolean {
        val file = File(path)
        return if (file.exists() && fileHaveContent(file)) {
            true
        } else {
            file.delete()
            false
        }
    }

    /**
     * Function that retrurn yesterday date
     */
    @SuppressLint("SimpleDateFormat")
    fun getYesterDayDate(): String {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DATE, -1)

        return SimpleDateFormat("dd_MM_yyyy").format(calendar.time)
    }

    /**
     * Push application's log on elasticsearch
     */
    @SuppressLint("SimpleDateFormat")
    fun pushLogsOnElasticSearch(logLevelName: String, logMessage: String) {
        doAsync {
            //we create a TCPClient object
            mTCPClient = TCPClient(object : TCPClient.OnMessageReceived {
                //here the messageReceived method is implemented
                override fun messageReceived(message: String) {
                    //this method calls the onProgressUpdate
                    //publishProgress(message)
                }
            })
            mTCPClient!!.run()
        }
        if (!firstTime) {
            Thread.sleep(1000)
            firstTime = true
        }
        if (mTCPClient != null) {
            val dt = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(Calendar.getInstance().time)
            val log = "$dt [rightplayer_mobile] $logLevelName : $logMessage"
            mTCPClient!!.sendMessage(log)
            Log.i(HLTAG, "sending message to TCP server | $log")
//            SendMessageTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, log)
        } else {
            Log.i(HLTAG, "mTCPCLIENT is null.")
        }
    }

    fun shellExecutor(command: String) {
        try {
            val process = Runtime.getRuntime().exec(command)
            process.waitFor()
            Log.i("process_exec".toUpperCase(), BufferedReader(InputStreamReader(process.inputStream)).readLines().toString())
        } catch (e: Exception) { HyperLog.e(HLTAG, "${e.message} : ${HLTAG.toUpperCase()}")}
    }

    /**
     * Remove all player's data
     */
    fun removePlayerData() {
        prefs.playerId = null
        prefs.playerCode = null
        prefs.playerMode = null
        prefs.playerStatus = null
        prefs.socketOn = null
        prefs.campaign = null
        prefs.campaignPath = null
        player.hasJoinRoom = false
        FileUtils.deleteDirectory(appStorage)
    }

    /**
     * Modify app logs directory content
     */
    @SuppressLint("SimpleDateFormat")
    fun modifyLogsDirectoryContent() {
        if (logsFileStorage.exists() && logsFileStorage.isDirectory) {
            val listFiles = logsFileStorage.listFiles() // Get the directory file count
            if (listFiles.size > 10) {
                val count = listFiles.size - 10
                val dateList: MutableList<Date> = ArrayList()
                for (file in listFiles) {
                    dateList.add(Date(file.lastModified()))
                }
                dateList.sort()
                for (i in 0..(count - 1)) {
                    val calendar = Calendar.getInstance()
                    calendar.time = dateList[i]
                    calendar.add(Calendar.DATE, -1)
                    val sdf = SimpleDateFormat("dd_MM_yyyy").format(calendar.time)
                    for (file in listFiles) {
                        if (file.name.contains(sdf)) {
                            HyperLog.i(HLTAG, "Deleting log file ${file.absolutePath} : ${HLTAG.toUpperCase()}")
                            pushLogsOnElasticSearch(Constant.i, "Deleting log file ${file.absolutePath} : ${HLTAG.toUpperCase()}")
                            if (file.delete()) {
                                HyperLog.i(HLTAG, "Zipped log file successfully deleted. : ${HLTAG.toUpperCase()}")
                                pushLogsOnElasticSearch(Constant.i, "Zipped log file successfully deleted. : ${HLTAG.toUpperCase()}")
                            } else {
                                HyperLog.i(HLTAG, "Failed to delete zipped log file. : ${HLTAG.toUpperCase()}")
                                pushLogsOnElasticSearch(Constant.e, "Failed to delete zipped log file. : ${HLTAG.toUpperCase()}")
                            }
                        }
                    }
                }
                HyperLog.i(HLTAG, "App logs directory content has been modify. : ${HLTAG.toUpperCase()}")
                pushLogsOnElasticSearch(i,"App logs directory content has been modify. : ${HLTAG.toUpperCase()}")
            }
        }
    }

    class TCPConnectTask: AsyncTask<String, String, TCPClient>() {

        override fun doInBackground(vararg params: String?): TCPClient? {
            //we create a TCPClient object
            mTCPClient = TCPClient(object : TCPClient.OnMessageReceived {
                //here the messageReceived method is implemented
                override fun messageReceived(message: String) {
                    //this method calls the onProgressUpdate
                    publishProgress(message)
                }
            })
            mTCPClient!!.run()
            return null
        }

        override fun onProgressUpdate(vararg values: String?) {
            super.onProgressUpdate(*values)
            //response received from server
            Log.i(HLTAG, "TCPConnectTask onProgressUpdate function => " + values[0])
        }
    }

    class SendMessageTask : AsyncTask<String, Void, Void>() {

        override fun doInBackground(vararg params: String): Void? {

            // send the message
            if (mTCPClient != null) {
                mTCPClient!!.sendMessage(params[0])
                Log.i(HLTAG, "Sending log message in async task : ${params[0]}.")
            }
            return null
        }

        override fun onPostExecute(nothing: Void) {
            super.onPostExecute(nothing)
        }
    }

}