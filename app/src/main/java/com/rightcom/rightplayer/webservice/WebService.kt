package com.rightcom.rightplayer.webservice

import com.rightcom.rightplayer.models.Models
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Abdel-Faïçal on 19/07/18.
 */
interface WebService {

    @get:GET("json/")
    val userLocation: Call<Models.IpInfo>

    @GET("check_or_generate_code")
    fun generateCode(
            @Query ("playerid") playerid: String,
            @Query ("action") action: String,
            @Query ("device") device: String,
            @Query ("player") player: String,
            @Query ("os") os: String,
            @Query ("playertype") playertype: String,
            @Query ("signature") signature: String,
            @Query ("ip") ip: String?,
            @Query ("versionCode") versionCode: String,
            @Query ("versionName") versionName: String,
            @Query ("releaseDate") releaseDate: String
    ): Call<Any>

    @GET("check_or_generate_code")
    fun checkCode(
            @Query ("playerid") playerid: String,
            @Query ("action") action: String,
            @Query ("signature") signature: String,
            @Query ("versionCode") versionCode: String,
            @Query ("versionName") versionName: String,
            @Query ("releaseDate") releaseDate: String
    ): Call<Any>

    @Headers("Content-Type: application/json")
    @POST("/")
    fun pushLogOnElasticSearch(@Body log: String): Call<Any>
}
