package com.rightcom.rightplayer.webservice

import com.google.gson.GsonBuilder
import com.rightcom.rightplayer.tools.Constant.URL_BASE
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Abdel-Faïçal on 19/07/18.
 */
object WebServiceUtils {

    val webserviceManager: WebService
        get() {
            val retrofit = Retrofit.Builder()
                    .baseUrl(URL_BASE)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            return retrofit.create(WebService::class.java)
        }

    fun webserviceManager(baseUrl: String): WebService {
        val gson = GsonBuilder()
                .setLenient()
                .create()
        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        return retrofit.create(WebService::class.java)
    }
}
